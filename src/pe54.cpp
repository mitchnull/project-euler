#include <iostream>
#include <boost/lexical_cast.hpp>

#include "hand.hpp"

using namespace Poker;

namespace {
	void report(int winner, const Hand &wh, const Hand &lh) {
		std::cout << "P" << winner << " wins with " << wh.ranking() << ": " << wh.best() << " over " << lh.ranking() << ": "  << lh.best() << std::endl;
	}
}

int main(int argc, const char **argv) {

	Cards c1(5), c2(5);

	int res = 0;
	while ((std::cin >> c1) && (std::cin >> c2)) {
		Hand h1(c1), h2(c2);
		if (Hand::lower(h2, h1)) {
			++res;
			report(1, h1, h2);
		}
		else {
			report(2, h2, h1);
		}
	}
	std::cout << "Player1 won " << res << " games" << std::endl;
	return 0;
}
