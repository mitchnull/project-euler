#ifndef POKER_CARDS_HPP
#define POKER_CARDS_HPP

#include <iostream>
#include <vector>
#include "card.hpp"

namespace Poker {
	class Cards : public std::vector<Card> {
		typedef std::vector<Card> Base_;

	public:
		Cards() {}

		explicit Cards(Base_::size_type size) : Base_(size) {}

		Cards(const Cards &oc) : Base_(oc) {}

		Cards(const Cards &table, const Cards &hand) {
			reserve(table.size() + hand.size());
			std::copy(table.begin(), table.end(), begin());
			std::copy(hand.begin(), hand.end(), begin() + table.size());
		}

		template<typename It>
		Cards(It b, It e) : Base_(b, e) {}

		friend std::ostream &operator<<(std::ostream &os, const Cards &cards) {
			bool isFirst = true;
			for (Cards::const_iterator i = cards.begin(); i != cards.end(); ++i) {
				if (isFirst)
					isFirst = false;
				else
					os << ' ';
				os << (*i);
			}
			return os;
		}

		friend std::istream &operator>>(std::istream &is, Cards &cards) {
			for (Cards::size_type i = 0; is && i < cards.size(); ++i)
				is >> cards[i];
			return is;
		}

		operator bool() const {
			for (const_iterator i = begin(); i != end(); ++i)
				if (!(*i))
					return false;
			return true;
		}

	};
}

#endif
