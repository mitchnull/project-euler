#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include <vector>
#include "primes.hpp"
#include "math.hpp"

namespace {
	template <typename T>
	bool isPaliN(T number, int n) {
		int bits = 1;
		while (number != 0) {
			const int d = 1 << (number % 10);
			if (bits & d)
				return false;
			bits |= d;
			number /= 10;
		}
		return bits == (1 << ++n) - 1;
	}

}

int
main(int argc, const char **argv) {
	int n = 9;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}

	while (((n*(n+1))/2) % 3 == 0) {
		std::cout << "skipping " << n << std::endl;
		--n;
	}
	Primes_t<int> primes;
	int max = 0;
	for (int i = n; i > 0; --i) {
		max *= 10;
		max += i;
	}
	primes.check(max); // calc bigest prime
	std::cout << "checking " << primes.size() << " primes, max: " << primes[primes.size() - 1] << std::endl;
	for (int i = primes.size() - 1; i >= 0; --i) {
		int p = primes[i];
		if (isPaliN(p, numDigits(p))) {
			std::cout << p << std::endl;
			break;
		}
	}
	return 0;
}
