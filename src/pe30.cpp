#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include "math.hpp"

typedef  boost::uint64_t Number;

namespace {
	template<typename T>
	T sumDPow(T a, int p) {
		T res = 0;
		while (a > 0) {
			res += pow(a%10, p);
			a /= 10;
		}
		return res;
	}

	template<typename T>
	T upperLimit(int p) {
		T a = 9;
		while (a <= sumDPow(a, p))
			a = 10 * a + 9;
		return a;
	}
}

int
main(int argc, const char **argv) {

	int n = 5;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	if (n > 9) {
		std::cerr << "n must be < 10" << std::endl;
		return -1;
	}
	Number ulimit = upperLimit<Number>(n);
	std::cout << "ulimit: " << ulimit << std::endl;
	Number sum = 0;
	for (Number i = 10; i <= ulimit; ++i) {
		if (i == sumDPow(i, n)) {
			sum += i;
			std::cout << i << std::endl;
		}
	}
	std::cout << "SUM = " << sum << std::endl;
	return 0;
}
