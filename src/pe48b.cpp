#include <iostream>
#include <sstream>
#include <boost/cstdint.hpp>

typedef boost::uint64_t Number;

namespace {
	template <typename T>
	T powMod(const T &base, const T &exp, const T &mod) {
		T res = 1;
		for (T i = 0; i < exp; ++i) {
			res *= base;
			res %= mod;
		}
		return res;
	}
}

int
main(int argc, const char **argv) {

	Number n = 1000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	const Number mod = 10000000000LL;
	Number sum = 0;
	for (Number i = 1; i <= n; ++i) {
		sum += powMod(i, i, mod);
		sum %= mod;
	}
	std::cout << "LD_SUM(i^i)[" << n << "] = " << sum << std::endl;
	return 0;
}
