#include <iostream>
#include <algorithm>
#include <boost/cstdint.hpp>
#include <boost/lexical_cast.hpp>
#include "primes.hpp"


namespace {
	typedef boost::uint64_t Number;

	Primes_t<Number> primes;

	bool isSameDigits(Number a, Number b, Number c) {
		std::string as = boost::lexical_cast<std::string>(a);
		std::string bs = boost::lexical_cast<std::string>(b);
		std::string cs = boost::lexical_cast<std::string>(c);
		std::sort(as.begin(), as.end());
		std::sort(bs.begin(), bs.end());
		std::sort(cs.begin(), cs.end());
		return as == bs && as == cs;
	}
}

int
main(int argc, const char **argv) {
	primes.check(999);
	Primes::size_type begin = primes.size() + 1;
	primes.check(9999);
	Primes::size_type end = primes.size() + 1;
	for (Primes::size_type i = begin; i < end; ++i) {
		const Number p = primes[i];
		for (Primes::size_type j = i + 1; j < end; ++j) {
			const Number pp = primes[j];
			const Number d = pp - p;
			const Number ppp = pp + d;
			if (!primes.check(ppp))
				continue;
			if (isSameDigits(p, pp, ppp)) {
				std::cout << p << pp << ppp << std::endl;
			}
		}
	}
	return 0;
}
