#include <iostream>
#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <map>
#include "math.hpp"
#include <cctype>

namespace {
	typedef mpz_class Number;

	std::size_t len(Number n) {
		return boost::lexical_cast<std::string>(n).size();
	}
}

int main(int argc, const char **argv) {


	std::size_t res = 1; // 1^1
	bool finish = false;

	for(uint n = 1; !finish; ++n) {
		finish = true;
		for (Number a = 2; a < 10; ++a) {
			if (len(pow(a, n)) == n) {
				finish = false;
				++res;
				std::cout << a << "^" << n << " = " << pow(a, n) << ": " << res << std::endl;
			}
		}
	}

	std::cout << res << std::endl;
	return 0;
}
