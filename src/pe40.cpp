#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include "math.hpp"

typedef boost::uint64_t Number;

namespace {

	template<typename T>
	int nthDigit(const T &num, const T &n) {
		return (num / pow(10, n)) % 10;
	}

}

int
main(int argc, const char **argv) {
	int n = 7;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Number res = 1;
	Number len = 0;
	Number c = 0;
	for (int i = 0; i < n; ++i) {
		Number di = pow(10, i);
		while (len < di)
			len += numDigits(++c);
		const int d = nthDigit(c, len - di);
		std::cout << "D(" << di << ") = " << d << std::endl;
		res *= d;
	}
	std::cout << res << std::endl;
	return 0;
}
