#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include "primes.hpp"

namespace {
	typedef boost::uint64_t Number;
	typedef std::pair<Primes::value_type, int> PL;

	Primes primes;

	PL findMax(Primes::size_type begin, Number max) {
		PL res;
		Number sum = primes[begin];
		for (int len = 2; (sum += primes[++begin]) < max; ++len) {
			if (primes.check(sum)) {
				res.first = sum;
				res.second = len;
			}
		}
		return res;
	}
}

int main(int argc, const char **argv) {

	Number n = 1000000;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}

	primes.sieve(n);
	Primes::size_type end = primes.size() + 1;
	Primes::value_type resp = 0;
	int resl = 0;
	for (Primes::size_type i = 0; i < end; ++i) {
		PL cm = findMax(i, n);
		if (cm.second > resl) {
			resp = cm.first;
			resl = cm.second;
		}
	}
	std::cout << resp << ", " << resl << std::endl;
	return 0;
}
