#include <iostream>
#include <sstream>
#include <boost/cstdint.hpp>
#include <boost/lexical_cast.hpp>
#include <gmpxx.h>
#include "math.hpp"

typedef mpz_class Number;

namespace {

	int sumDigits(Number a) {
		std::string as = a.get_str();
		int res = 0;
		for (std::string::const_iterator i = as.begin(); i != as.end(); ++i)
			res += *i - '0';
		return res;
	}
}

int main(int argc, const char **argv) {
	int n = 100;
	if (argc > 1) {
		n = boost::lexical_cast<int>(argv[1]);
	}
	int res = 0;
	for (Number a = 1; a < n; ++a) {
		for (Number b = 1; b < n ; ++b) {
			int dsum = sumDigits(pow(a, b));
			if (dsum > res)
				res = dsum;
		}
	}
	std::cout << res << std::endl;
	return 0;
}
