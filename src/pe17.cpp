#include <iostream>
#include <sstream>
#include <boost/cstdint.hpp>
#include <gmpxx.h>
#include <vector>

namespace {

	const char * pd[] = {
		"",
		"one",
		"two",
		"three",
		"four",
		"five",
		"six",
		"seven",
		"eight",
		"nine",
		"ten",
		"eleven",
		"twelve",
		"thirteen",
		"fourteen",
		"fifteen",
		"sixteen",
		"seventeen",
		"eighteen",
		"nineteen"
	};

	const char *tens[] = {
		"",
		"",
		"twenty",
		"thirty",
		"forty",
		"fifty",
		"sixty",
		"seventy",
		"eighty",
		"ninety"
	};

	std::string pronounce(int n) {
		if (n < 20)
			return pd[n];
		if (n < 100)
			return std::string(tens[n / 10]) + pd[n % 10];
		if (n < 1000) {
			std::string rest = pronounce(n%100);
			return std::string(pd[n / 100]) + (rest.empty() ? "hundred" : "hundredand") + rest;
		}
		return "onethousand";
	}
}

int
main(int argc, const char **argv) {

	int n = 1000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	std::cout << n << " = " << pronounce(n) << std::endl;
	std::cout << "Letters(" << n << ") = " << pronounce(n).size() << std::endl;
	int res = 0;
	for (int i = 1; i <= n; ++i)
		res += pronounce(i).size();
	std::cout << "SumLetters(" << n << ") = " << res << std::endl;
	return 0;
}
