#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include "primes.hpp"

namespace {
	typedef boost::uint64_t Number;
	typedef Primes_t<Number> P;

	P primes;
}

int main(int argc, const char **argv) {

	Number n = 5000;
	int m = 1;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}
	if (argc > 2) {
		m = boost::lexical_cast<int>(argv[2]);
	}

	primes.sieve(n);
	Number n2 = n;
	std::cout << "sieve done" << std::endl;
	for (int i = 0; i < m; ++i) {
		Number sum = 0;
		for (Number i = 0; i < n2; ++i)
			if (primes.isPrime(i))
				++sum;

		std::cout << sum << std::endl;
	}

	std::cout << n << std::endl;
	return 0;
}
