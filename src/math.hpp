#ifndef PE_MATH_HPP
#define PE_MATH_HPP

#include <cmath>
#include <gmpxx.h>
#include <boost/lexical_cast.hpp>
#include <string>
#include <boost/rational.hpp>

namespace std {
	template <>
	struct numeric_limits<mpz_class>
	{
		static const bool is_specialized = true;

		static const bool is_signed = true;
		static const bool is_integer = true;
		static const bool is_exact = true;
		static const bool has_infinity = false;
		static const bool has_quiet_NaN = false;
		static const bool has_signaling_NaN = false;
		static const bool has_denorm_loss = false;

                static const int digits10 = 0;
                static const int digits = 0;
                static const int radix = 2;

		static const bool is_iec559 = false;
		static const bool is_bounded = false;
		static const bool is_modulo = false;

		static const bool traps = false;
		static const bool tinyness_before = false;
		static const float_round_style round_style = round_toward_zero;
	};
}

#if 0
namespace boost {
	template<>
	std::string lexical_cast(const mpz_class &n) {
		return n.get_str();
	}
	template<>
	mpz_class lexical_cast(const std::string &s) {
		return mpz_class(s);
	}
        // FIXME: this is an ugly hack, see https://svn.boost.org/trac/boost/ticket/4139
	namespace math { namespace detail {
		template<>
		mpz_class
		gcd_integer(const mpz_class &a, const mpz_class &b) {
			// Avoid repeated construction
			mpz_class const zero = static_cast<mpz_class>( 0 );
			mpz_class const result = gcd_euclidean( a, b );

			if (result < zero) {
				return -result;
			}
			else {
				return result;
			}
		}
	}}
}
#endif

template <typename T, typename U>
T pow(const T &base, const U &exp) {
	T res = 1;
	for (U i = 0; i < exp; ++i)
		res *= base;
	return res;
}

template <typename T, typename U, typename V>
T powMod(const T &base, const U &exp, const V &mod) {
	T res = 1;
	for (U i = 0; i < exp; ++i) {
		res *= base;
		res %= mod;
	}
	return res;
}

template <typename T>
T factorial(const T &n) {
	T res = 1;
	for (T i = 2; i <= n; ++i)
		res *= i;
	return res;
}

template<typename T>
T nk(const T &n, const T &k) {
	const T nmk = n - k;
	if (nmk < k)
		return nk(n, nmk);
	T res = 1;
	for (T i = n; i > nmk; --i)
		res *= i;
	return res / factorial(k);
}

template<typename T>
T numDigits(T n, const int base = 10) {
	T res = 0;
	do {
		++res;
	} while ((n /= base) != 0);
	return res;
}

template<typename T>
T truncLeft(const T &n, const int base = 10) {
	return n % (pow(base, numDigits(n) - 1));
}

template<typename T>
T truncRight(const T &n, const int base = 10) {
	return n / base;
}

template<typename Int>
Int isqrt(const Int &v) {
	return static_cast<Int>(std::sqrt(v + 0.5));
}

template<>
mpz_class isqrt(const mpz_class &v) {
	return sqrt(v);
}


template<typename T>
T isSquare(const T &n) {
	const T sn = isqrt(n);
	return sn*sn == n;
}

template<typename T>
T isCube(const T &n) {
	const T cn = static_cast<T>(std::pow(n, 1.0/3.0) + 0.5);
	return pow(cn, 3) == n;
}

#endif

