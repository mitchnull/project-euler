#include <iostream>
#include <algorithm>
#include <sstream>
#include "primes.hpp"
#include <set>

typedef Primes::value_type Number;

namespace {
	template<typename T>
	T sumProperDivs(T n) {
		static Primes_t<T> primes;
		return primes.sumProperDivisors(n);
	}

	template<typename T>
	T checkSum2(const typename std::set<T> &ab, T x) {
		T half = x / 2;
		typename std::set<T>::const_iterator abEnd = ab.end();
		for (typename std::set<T>::const_iterator i = ab.begin(); i != abEnd; ++i) {
			if (ab.find(x - *i) != abEnd)
				return true;
			if (*i > half)
				return false;
		}
		return false;
	}
}

int
main(int argc, const char **argv) {

	Number n = 28123;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}

	std::set<Number> abundant;
	Number res = 0;
	for (Number i = 1; i <= n; ++i) {
		if (sumProperDivs(i) > i)
			abundant.insert(i);
		if (!checkSum2(abundant, i)) {
//			std::cout << i << std::endl;
			res += i;
		}
	}
	std::cout << "SumNotA+B(" << n << ") = " << res << std::endl;
	return 0;
}
