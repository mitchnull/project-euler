#include <iostream>
#include <sstream>
#include <gmpxx.h>

int
main(int argc, const char **argv) {

	int n = 10;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	mpz_class curr, sum = 0;
	std::string s;

	while (std::cin >> s) {
		curr = s;
		sum += curr;
	}
	std::stringstream ts;
	ts << sum;
	std::string ssum = ts.str();
	std::cout << ssum.substr(0, n) << std::endl;
	return 0;
}
