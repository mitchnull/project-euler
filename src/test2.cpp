#include <iostream>
#include <sstream>
#include <boost/cstdint.hpp>

typedef boost::uint64_t Number;

namespace {
	template <typename T>
	std::string itoa(const T &n) {
		std::stringstream ss;
		ss << n;
		return ss.str();
	}
	template <typename T>
	T atoi(const std::string &s) {
		std::stringstream ss;
		ss.str(s);
		T res;
		ss >> res;
		return res;
	}
	template <typename T>
	void atoi(const std::string &s, T &n) {
		std::stringstream ss;
		ss.str(s);
		ss >> n;
	}
}

int main(int argc, const char **argv) {

	Number n = 1000;

	if (argc > 1) {
		n = atoi<Number>(argv[1]);
	}

	Number sum = 0;
	for (Number i = 0; i < n; ++i) {
		std::string s = itoa(i);
		Number k;
		atoi(s, k);
		sum += k;
	}
	std::cout << sum << std::endl;
	return 0;
}
