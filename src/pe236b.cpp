#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <boost/rational.hpp>
#include <cctype>
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <bitset>

namespace {
	typedef boost::uint64_t Number;
	typedef boost::rational<Number> Rat;
	typedef std::vector<Number> NumVec;
	typedef std::pair<Number, Number> NumPair;
	typedef std::vector<NumPair> NumPairVec;
	typedef std::set<Rat, std::greater<Rat> > Candidates;

	template<typename T, size_t n>
	char (& as_char_array(const T (&)[n]) )[n];


#if 0
	NumPair Prods[] = {
		NumPair(5248, 640),
		NumPair(1312, 1888),
		NumPair(2624, 3776),
		NumPair(5760, 3776),
		NumPair(3936, 5664)
	};
#endif
	NumPair Prods[] = {
		NumPair(5248, 640),
		NumPair(5760, 3776),
		NumPair(1312 + 2624 + 3936, 1888 + 3776 + 5664),
	};

	enum { NumProds = sizeof(as_char_array(Prods)) };

	class Counter : public std::vector<int> {
		typedef std::vector<int> Base_;
		public:
			Counter() : Base_(NumProds) {}
		friend std::ostream &operator<<(std::ostream &os, const Counter &c) {
			os << "(" << c[0];
			for (Counter::const_iterator i = c.begin() + 1; i != c.end(); ++i)
				os << "," << *i;
			os << ")";
			return os;
		}
	};
	typedef std::set<Counter> CSet;
	typedef std::vector<CSet> CSetVec;
	typedef std::vector<Counter> CVec;
	class BitSet : public std::bitset<NumProds> {
	public:
		BitSet &operator+=(const BitSet &bs) {
			for (std::size_t p = 0; p < NumProds; ++p)
				if (bs.test(p))
					set(p);
			return *this;
		}
	};
	typedef std::vector<BitSet> BitSetVec;

	NumPair Sum;


	struct Identity {
		Number operator()(const Number &n) const { return n; }
	} SelIdentity;
	struct First {
		Number operator()(const NumPair &p) const { return p.first; }
	} SelFirst;
	struct Second {
		Number operator()(const NumPair &p) const { return p.second; }
	} SelSecond;

	template<typename Selector>
	Rat pps(Selector ps, std::size_t prod, Number spoil) {
		return Rat(spoil, ps(Prods[prod]));
	}

	template<typename It, typename Selector>
	Number sum(It begin, It end, Selector ps = SelIdentity) {
		Number res = 0;
		while (begin != end)
			res += ps(*(begin++));
		return res;
	}

	bool checkMinSpoil(std::size_t prod, const Rat &m) {
		const Rat mm = m * Prods[prod].second / Prods[prod].first;
		return (mm.denominator() <= Prods[prod].first
				&& mm.numerator() <= Prods[prod].second);
	}

	bool checkMinSpoils(const Rat &m, std::size_t first = 0) {
		while (first < NumProds) {
			if (!checkMinSpoil(first++, m))
				return false;
		}
		return true;
	}

	NumPair minSpoil(std::size_t prod, const Rat &m) {
		NumPair res;
		const Rat mm = m * Prods[prod].second / Prods[prod].first;
		if (mm.denominator() <= Prods[prod].first
				&& mm.numerator() <= Prods[prod].second) {
			res.first = mm.denominator();
			res.second = mm.numerator();
		}
		return res;
	}

	bool check(const NumPairVec &spoils, const Counter &c, Number sSum) {
		for (std::size_t p = 0; p < NumProds; ++p) {
			if (!c[p])
				return false;
			sSum -= c[p] * spoils[p].second;
		}
		return sSum == 0;
	}

	bool checkMaybe(const Rat &m) {
		NumPairVec spoils(NumProds);
		for (std::size_t i = 0; i < NumProds; ++i) {
			const NumPair sp = minSpoil(i, m);
			if (sp.first == 0)
				return false;
			spoils[i] = sp;
		}
		const Rat mm = m * Sum.first / Sum.second;
		const Number fSum = mm.numerator();
		const Number sSum = mm.denominator();
		const Number fMax = (Sum.first / fSum) * fSum;
		const Number sMax = (Sum.second / sSum) * sSum;
//		std::cout << m << " (" << mm << "): " << fMax << "/" << Sum.first << ", " << sMax << "/" << Sum.second << std::endl;
		BitSetVec fSums(fMax + 1);
		BitSetVec sSums(sMax + 1);
		for (std::size_t p = 0; p < NumProds; ++p) {
			fSums[spoils[p].first].set(p);
			sSums[spoils[p].second].set(p);
		}
		for (BitSetVec::size_type i = 1; i < fMax ; ++i) {
			BitSet &bs = fSums[i];
			if (!bs.any())
				continue;
			for (std::size_t p = 0; p < NumProds; ++p) {
				if (i + spoils[p].first > fMax)
					continue;
				BitSet &nbs = fSums[i + spoils[p].first];
				nbs += bs;
				nbs.set(p);
			}
		}
		for (BitSetVec::size_type i = 1; i < sMax ; ++i) {
			BitSet &bs = sSums[i];
			if (!bs.any())
				continue;
			for (std::size_t p = 0; p < NumProds; ++p) {
				if (i + spoils[p].second > sMax)
					continue;
				BitSet &nbs = sSums[i + spoils[p].second];
				nbs += bs;
				nbs.set(p);
			}
		}

		for (Number fs = fSum, ss = sSum;  fs <= fMax && ss <= sMax; fs += fSum, ss += sSum) {
			if (fSums[fs].count() == NumProds && sSums[ss].count() == NumProds)
				return true;
		}
//		std::cout << fSum << "/" << sSum << ": FAIL-MAYBE" << std::endl;
		return false;
	}

	typedef std::vector<std::size_t> IdxVec;

	bool inc(NumPairVec &spoils, const NumPairVec &minSpoils, const IdxVec &idxs) {
		for (IdxVec::const_iterator i = idxs.begin(); i != idxs.end(); ++i) {
			const std::size_t p = *i;
			if (spoils[p].first + minSpoils[p].first <= Prods[p].first
					&& spoils[p].second + minSpoils[p].second <= Prods[p].second) {
				spoils[p].first += minSpoils[p].first;
				spoils[p].second += minSpoils[p].second;
				return true;
			}
			spoils[p] = minSpoils[p];
		}
		return false;
	}

	bool check2(const Rat &m) {
		(std::cout << "\rchecking " << m << "...                          \r").flush();
		NumPairVec minSpoils(NumProds);
		for (std::size_t i = 0; i < NumProds; ++i) {
			const NumPair sp = minSpoil(i, m);
			if (sp.first == 0)
				return false;
			minSpoils[i] = sp;
		}

		NumPairVec pv(NumProds);
		for (std::size_t p = 0; p < NumProds; ++p) {
			pv[p] = NumPair(minSpoils[p].first * minSpoils[p].second, p);
		}
		std::sort(pv.begin(), pv.end(), std::greater<NumPair>());
		IdxVec idxs(NumProds);
		for (std::size_t p = 0; p < NumProds; ++p) {
			idxs[p] = pv[p].second;
//			std::cout << pv[p].second << ": " << pv[p].first << ", " << minSpoils[pv[p].second].first << ", " << minSpoils[pv[p].second].second << std::endl;
		}

		const Rat mm = m * Sum.first / Sum.second;
		NumPairVec spoils(minSpoils);
		do {
			Number mn = 0;
			Number md = 0;
			for (std::size_t p = 0; p < NumProds; ++p) {
				mn += spoils[p].first;
				md += spoils[p].second;
			}
			if (mm == Rat(mn, md)) {
				std::cout << std::endl;
				return true;
			}
		} while (inc(spoils, minSpoils, idxs));
		return false;
	}
}

int main(int argc, const char **argv) {

	Rat start;
	if (argc > 1) {
		start = boost::lexical_cast<Rat>(argv[1]);
	}

	std::sort(&Prods[0], &Prods[NumProds]);
	Sum.first = sum(&Prods[0], &Prods[NumProds], SelFirst);
	Sum.second = sum(&Prods[0], &Prods[NumProds], SelSecond);

#if 0
	if (check2(Rat(1476, 1475))) {
		std::cout << "SUCCESS" << std::endl;
		return 0;
	}
#endif

	Candidates candidates;
	for (Number i = 1; i <= Prods[0].first; ++i) {
		const Rat pp00 = pps(SelFirst, 0, i);
		for (Number j = 1; j <= Prods[0].second; ++j) {
			const Rat pp10 = pps(SelSecond, 0, j);
			if (pp10 <= pp00) // TODO: put it in loop start/stop condition
				continue;
			Rat m = pp10 / pp00;
			if (checkMinSpoils(m, 1))
				candidates.insert(m);
		}
	}
	std::cout << candidates.size() << ", "  << *candidates.begin() << " - " << *candidates.rbegin() << ": " << std::distance(candidates.lower_bound(Rat(1476, 1475)), candidates.end()) << std::endl;
	for (Candidates::const_iterator i = (start != 0 ? candidates.lower_bound(start) : candidates.begin()); i != candidates.end(); ++i) {
		if (checkMaybe(*i) && check2(*i)) {
			std::cout << "m = " << (*i) << std::endl;
			if (false)
				return 0;
		}
	}
	return 0;
}
