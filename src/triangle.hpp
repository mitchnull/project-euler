#ifndef PE_TRIANGLE_HPP
#define PE_TRIANGLE_HPP

#include <vector>
#include <iostream>

template<typename T>
class Triangle {
private:
	typedef typename std::vector<T> PV;
public:
	typedef typename PV::size_type size_type;
	typedef T value_type;
	typedef typename PV::iterator iterator;
	typedef typename PV::const_iterator const_iterator;

private:
	size_type rows_;
	PV v_;

	static size_type rowOffset(int row) {
		return (row * (row + 1)) / 2;
	}

	static size_type rowSize(int row) {
		return row + 1;
	}

public:

	Triangle(size_type rows) : rows_(rows), v_(rowOffset(rows)) {}

	T &operator()(size_type i, size_type j) {
		return v_[rowOffset(i) + j];
	}

	const T &operator()(size_type i, size_type j) const {
		return v_[rowOffset(i) + j];
	}

	const_iterator begin() const {
		return v_.begin();
	}

	const_iterator end() const {
		return v_.end();
	}

	iterator begin() {
		return v_.begin();
	}

	iterator end() {
		return v_.end();
	}

	size_type rows() const { return rows_; }

//	friend std::istream &operator>>(std::istream &is, Matrix<T> &m);
//	friend std::ostream &operator<<(std::ostream &os, const Matrix<T> &m);

};

template<typename T>
std::istream &operator>>(std::istream &is, Triangle<T> &t) {
	for (typename Triangle<T>::size_type i = 0; i < t.rows(); ++i) {
		for (typename Triangle<T>::size_type j = 0; j <= i ; ++j) {
			is >> t(i, j);
		}
	}
	return is;
}

template<typename T>
std::ostream &operator<<(std::ostream &os, const Triangle<T> &t) {
	for (typename Triangle<T>::size_type i = 0; i < t.rows(); ++i) {
		bool isFirst = true;
		for (typename Triangle<T>::size_type j = 0; j <= i; ++j) {
			if (isFirst) {
				isFirst = false;
			}
			else {
				os << ' ';
			}
			os << t(i, j);
		}
		os << std::endl;
	}
	return os;
}

#endif
