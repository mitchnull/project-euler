#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <vector>
#include "math.hpp"

typedef boost::uint64_t Number;

namespace {

	class DSet {
		bool bits_[10];
	public:
		DSet() {
			bits_[0] = true;
			std::fill(&bits_[1], &bits_[10], false);
		}
		bool isAllSet() const {
			for (int i = 1; i < 10; ++i)
				if (!bits_[i])
					return false;
			return true;
		}

		bool &operator[](int i) { return bits_[i]; }
		bool operator[](int i) const { return bits_[i]; }
	};

	bool update(DSet &ds, Number a) {
		while (a > 0) {
			int d = a % 10;
			if (ds[d])
				return false;
			ds[d] = true;
			a /= 10;
		}
		return true;
	}

	Number cpp(const Number &a, int n) {
		Number res = a;
		for (int i = 2; i <= n; ++i) {
			Number x = a * i;
			res *= pow(10, numDigits(x));
			res += x;
		}
		return res;
	}


	void print(Number a, int n) {
		std::cout << "CP(" << a << ", " << n << ") = " << cpp(a, n) << std::endl;
	}

	bool isCPP(const Number &a, int n) {
		DSet ds;
		for (int i = 1; i <= n; ++i) {
			Number ai = a * i;
			if (!update(ds, ai))
				return false;
		}
		if (ds.isAllSet()) {
			print(a, n);
			return true;
		}
		return false;
	}

	int numCPDigits(const Number &a, int n) {
		int res = 0;
		for (int i = 1; i <= n; ++i) {
			res += numDigits(a * i);
		}
		return res;
	}

}

int
main(int argc, const char **argv) {
	Number res = 0;
	Number resa = 0;
	int resn = 0;
	for (int i = 2; i <= 9; ++i) {
		for (Number a = 1; numCPDigits(a, i) < 10; ++a) {
			if (isCPP(a, i)) {
				Number tcpp = cpp(a, i);
				if (tcpp > res) {
					res = tcpp;
					resa = a;
					resn = i;
				}
			}
		}
	}
	std::cout << std::endl;
	print(resa, resn);
	return 0;
}
