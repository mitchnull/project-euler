#include <iostream>
#include <sstream>
#include <cmath>
#include <boost/cstdint.hpp>
#include <gmpxx.h>

namespace {
	template <typename T>
	T factorial(const T &n) {
		T res = 1;
		for (T i = 2; i <= n; ++i)
			res *= i;
		return res;
	}

	template<typename T>
	T nk(const T &n, const T &k) {
		const T nmk = n - k;
		if (nmk < k)
			return nk(n, nmk);
		T res = 1;
		for (T i = n; i > nmk; --i)
			res *= i;
		return res / factorial(k);
	}
}

int
main(int argc, const char **argv) {

	mpz_class n = 100;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	mpz_class nf = factorial(n);
	std::string nfs = nf.get_str();
	int res = 0;
	for (std::string::const_iterator i = nfs.begin(); i != nfs.end(); ++i) {
		res += *i - '0';
	}
	std::cout << "SumDigits(" << n << "!) = " << res << std::endl;
	return 0;
}
