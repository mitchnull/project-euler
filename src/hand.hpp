#ifndef POKER_HAND_HPP
#define POKER_HAND_HPP

#include <iostream>
#include <map>
#include <boost/lexical_cast.hpp>
#include "cards.hpp"

namespace Poker {

	class Hand {
		enum { FullHand = 5 };
		typedef enum { HighCard, OnePair, TwoPair, Set, Straight, Flush, FullHouse, Quads, StraightFlush, RoyalFlush } Ranking;
		typedef std::pair<Card::Rank, int> SameCount;

		Ranking ranking_;
		Cards cards_;

		Card::Suit checkFlush(Cards::const_iterator begin, Cards::const_iterator end) const {
			std::map<Card::Suit, int> sc;
			for (; begin != end; ++begin) {
				++sc[begin->suit];
			}
			for (std::map<Card::Suit, int>::const_iterator i = sc.begin(); i != sc.end(); ++i) {
				if (i->second >= FullHand)
					return i->first;
			}
			return Card::NullSuit;
		}

		void selectFlush(Cards::iterator begin, Cards::iterator end, Card::Suit s) {
			Cards::iterator dst = cards_.begin();
			for (int c = 0; c < FullHand && begin != end; ++c) {
				if (begin->suit == s)
					*dst++ = *begin++;
				else
					++begin;
			}
		}

		bool checkStraight(Cards::const_iterator begin, Cards::const_iterator end, Card::Suit s, Card::Rank startRank, int count) const {
			int r = startRank;
			while (count > 0 && begin != end) {
				if (s != Card::NullSuit) {
					if (begin->suit != s) {
						++begin;
						continue;
					}
				}
				else {
					if (begin->rank > r) {
						++begin;
						continue;
					}
				}
				if (begin->rank != r)
					return false;
				--count;
				++begin;
				--r;
			}
			return count == 0;
		}

		Cards::const_iterator checkStraight(Cards::const_iterator begin, Cards::const_iterator end, Card::Suit s = Card::NullSuit) const {
			for (Cards::const_iterator i = begin; i < end; ++i) {
				if (i->rank == Card::Five) {
					if (end - i < FullHand - 1)
						return end;
					if (checkStraight(i, end, s, i->rank, FullHand - 1)
							&& checkStraight(begin, end, s, Card::Ace, 1))
						return i;
				}
				else {
					if (end - i < FullHand)
						return end;
					if (checkStraight(i, end, s, i->rank, FullHand))
						return i;
				}
			}
			return end;
		}

		Cards::iterator selectStraight(Cards::const_iterator begin, Cards::const_iterator end, Card::Suit s, Card::Rank startRank, Cards::iterator dst, int count) {
			int r = startRank;
			while (count > 0 && begin != end) {
				if (s != Card::NullSuit) {
					if (begin->suit != s) {
						++begin;
						continue;
					}
				}
				else {
					if (begin->rank != r) {
						++begin;
						continue;
					}
				}
				*dst++ = *begin++;
				--count;
				--r;
			}
			return dst;
		}

		void selectStraight(Cards::iterator begin, Cards::iterator end, Cards::const_iterator start, Card::Suit s = Card::NullSuit) {
			if (start->rank == Card::Five) {
				Cards::iterator dst = selectStraight(start, end, s, start->rank, cards_.begin(), FullHand - 1);
				selectStraight(begin, end, s, Card::Ace, dst, 1);
			}
			else {
				selectStraight(start, end, s, start->rank, cards_.begin(), FullHand);
			}
		}

		SameCount checkSame(Cards::iterator begin, Cards::iterator end, Card::Rank ignore = Card::NullRank) {
			SameCount res;
			SameCount curr;
			for (; begin != end; ++begin) {
				if (begin->rank == ignore) {
					continue;
				}
				if (begin->rank != curr.first) {
					if (curr.second > 1 && curr.second > res.second)
						res = curr;
					curr.first = begin->rank;
					curr.second = 1;
					continue;
				}
				++curr.second;
			}
			if (curr.second > 1 && curr.second > res.second)
				res = curr;
			return res;
		}

		Cards::iterator selectSame(Cards::iterator begin, Cards::iterator end, Card::Rank r, int offset = 0) {
			Cards::iterator dst = cards_.begin() + offset;
			while (begin != end && begin->rank != r)
				++begin;
			Cards::iterator save = begin;
			while (begin != end && begin->rank == r)
				*dst++ = *begin++;
			return std::copy(begin, end, save);
		}

		void selectN(Cards::iterator begin, Cards::iterator end, int n) {
			for (Cards::iterator dst = cards_.begin() + (FullHand - n); n > 0 && begin != end; --n) {
				*dst++ = *begin++;
			}
		}

		Ranking eval(Cards cards) {
			Cards::iterator cend = cards.end();
			std::sort(cards.begin(), cend, std::greater<Card>());
			Card::Suit flush = checkFlush(cards.begin(), cend);
			Cards::const_iterator straight = checkStraight(cards.begin(), cend, flush);

			if (flush != Card::NullSuit && straight != cend) {
				selectStraight(cards.begin(), cend, straight, flush);
				return cards_.front().rank == Card::Ace ? RoyalFlush : StraightFlush;
			}
			SameCount sc = checkSame(cards.begin(), cend);
			if (sc.second == 4) {
				cend = selectSame(cards.begin(), cend, sc.first);
				selectN(cards.begin(), cend, FullHand - 4);
				return Quads;
			}
			if (sc.second == 3) {
				SameCount sc2 = checkSame(cards.begin(), cend, sc.first);
				if (sc2.second == 2) {
					cend = selectSame(cards.begin(), cend, sc.first);
					cend = selectSame(cards.begin(), cend, sc2.first, sc.second);
					return FullHouse;
				}
			}
			if (flush != Card::NullSuit) {
				selectFlush(cards.begin(), cend, flush);
				return Flush;
			}
			if (straight != cend) {
				selectStraight(cards.begin(), cend, straight);
				return Straight;
			}
			if (sc.second == 3) {
				cend = selectSame(cards.begin(), cend, sc.first);
				selectN(cards.begin(), cend, FullHand - 3);
				return Set;
			}
			if (sc.second == 2) {
				cend = selectSame(cards.begin(), cend, sc.first);
				SameCount sc2 = checkSame(cards.begin(), cend);
				if (sc2.second == 2) {
					cend = selectSame(cards.begin(), cend, sc2.first, sc.second);
					selectN(cards.begin(), cend, FullHand - (sc.second + sc2.second));
					return TwoPair;
				}
				selectN(cards.begin(), cend, FullHand - sc.second);
				return OnePair;
			}
			selectN(cards.begin(), cend, FullHand);
			return HighCard;
		}

	public:
		Hand(const Cards &cards) : cards_(FullHand) {
			ranking_ = eval(cards);
		}

		Ranking ranking() const { return ranking_; };

		const Cards &best() const {
			return cards_;
		}

		static bool lower(const Hand &h, const Hand &oh) {
			if (h.ranking_ < oh.ranking_)
				return true;
			if (h.ranking_ > oh.ranking_)
				return false;
			Cards::const_iterator hi = h.cards_.begin();
			Cards::const_iterator ohi = oh.cards_.begin();
			int c = 0;
			while (hi != h.cards_.end() && ohi != oh.cards_.end()) {
				if (hi->rank < ohi->rank)
					return true;
				if (hi->rank > ohi->rank)
					return false;
				if (++c >= FullHand)
					return false;
			}
			return h.cards_.size() < oh.cards_.size();
		}

		friend std::ostream &operator<<(std::ostream &os, const Ranking &ranking) {
			return os << toString(ranking);
		}

		static std::string toString(const Ranking ranking) {
			switch (ranking) {
				case HighCard: return "HighCard";
				case OnePair: return "OnePair";
				case TwoPair: return "TwoPair";
				case Set: return "Set";
				case Straight: return "Straight";
				case Flush: return "Flush";
				case FullHouse: return "FullHouse";
				case Quads: return "Quads";
				case StraightFlush: return "StraightFlush";
				case RoyalFlush: return "RoyalFlush";
			}
			return boost::lexical_cast<std::string>(ranking);
		}
	};
}

#endif
