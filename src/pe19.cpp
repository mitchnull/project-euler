#include <iostream>
#include <boost/date_time/gregorian/gregorian.hpp>

namespace d = boost::gregorian;

int
main(int argc, char **argv) {
	
	d::date start(1901, d::Jan, 1);
	d::date end(2000, d::Dec, 31);

	int res = 0;
	while (start < end) {
		if (start.day_of_week() == d::Sunday) {
			std::cout << start << std::endl;
			++res;
		}
		start += d::months(1);
	}
	std::cout << "Months starting with Sunday in the 20th century: " << res << std::endl;
	return 0;
}
