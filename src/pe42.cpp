#include <iostream>
#include <sstream>
#include <set>
#include <boost/cstdint.hpp>
#include <cmath>

namespace {

class Name {
	std::string str_;
	int score_;
public:
	Name() : score_(0) {}

	friend std::istream &operator>>(std::istream &is, Name &name) {
		name.str_.clear();
		name.score_ = 0;
		char ch;
		bool foundStart = false;
		while (is >> ch) {
			switch (ch) {
				case '"':
					if (foundStart)
						return is;
					else
						foundStart = true;
					break;
				case ',':
					break;
				default:
					name.str_ += ch;
					name.score_ += ch - 'A' + 1;
					break;
			}
		}
		return is;
	}

	int score() const { return score_; }
	std::string str() const { return str_; }

	bool operator<(const Name &other) const {
		return str_ < other.str_;
	}
};

	template<typename T>
	bool isTriangleNum(T number) {
		T n = static_cast<T>(std::sqrt(2*number));
		return number == (n * (n + 1)) / 2;
	}
}

int
main(int argc, const char **argv) {

	boost::uint64_t res = 0;

	Name name;
	while (std::cin >> name) {
		if (isTriangleNum(name.score())) {
			std::cout << name.str() << ": " << name.score() << std::endl;
			++res;
		}

	}
	std::cout << res << std::endl;
	return 0;
}
