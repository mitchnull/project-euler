#ifndef PE_PRIMES_HPP
#define PE_PRIMES_HPP

#include <vector>
#include <boost/cstdint.hpp>
#include <iterator>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <map>

template<typename T = boost::uint64_t>
class Primes_t {
private:
	typedef typename std::vector<T> PV;
	typedef typename PV::iterator PVI;
	typedef typename PV::const_iterator PVCI;

	PV primes_;
	T lastChecked_;

	bool checkNextPrime() {
		++lastChecked_;
		const T sqi = static_cast<T>(std::sqrt(lastChecked_)); 
		for (PVCI it = primes_.begin(); it != primes_.end(); ++it) {
			const T p = *it;
			if (p > sqi)
				break;
			if (lastChecked_ % p == 0)
				return false;
		}
		primes_.push_back(lastChecked_);
		return true;
	}

public:
	typedef T value_type;
	typedef PVCI const_iterator;
	typedef typename PV::size_type size_type;
	class Factors : public std::map<T, T> {
	public: 
		class DivIterator : public std::iterator<std::forward_iterator_tag, T> {
			const Factors *parent_;
			std::vector<T> counter_;
			T value_;

			DivIterator(const Factors &parent) : parent_(&parent), counter_(parent.size()), value_(1) {}
			friend class Factors;
		public:
			DivIterator() : parent_(0), counter_(), value_(0) {}
			DivIterator(const DivIterator &other) : parent_(other.parent_), counter_(other.counter_), value_(other.value_) {}
			DivIterator &operator=(const DivIterator &other) {
				if (&other == this)
					return *this;
				parent_ = other.parent_;
				counter_ = other.counter_;
				value_ = other.value_;
				return *this;
			}
			bool operator==(const DivIterator &other) {
				return parent_ == other.parent_ && value_ == other.value_;
			}
			bool operator!=(const DivIterator &other) {
				return !operator==(other);
			}
			DivIterator operator++(int) {
				DivIterator tmp(*this);
				++(*this);
				return tmp;
			}
			DivIterator &operator++() {
				if (!parent_)
					return *this;
				typename Factors::const_iterator fi = parent_->begin();
				int i = 0;
				for (typename Factors::const_iterator fi = parent_->begin(); fi != parent_->end(); ++fi) {
					if (++counter_[i] <= fi->second) {
						value_ *= fi->first;
						return *this;
					}
					while (--counter_[i] > 0)
						value_ /= fi->first;
					++i;
				}
				parent_ = 0;
				value_ = 0;
				return *this;
			}
			const T &operator*() {
				return value_;
			}
			const T *operator->() {
				return &value_;
			}
		};
	private:
		static const DivIterator DivsEnd_;
	public:
		DivIterator divsBegin() const { return DivIterator(*this); }
		DivIterator divsEnd() const { return DivIterator(); }

		friend std::ostream &operator<<(std::ostream &os, const Factors &factors) {
			bool isFirst = true;
			for (typename Factors::const_iterator it = factors.begin(); it != factors.end(); ++it) {
				if (isFirst)
					isFirst = false;
				else
					std::cout << ' ';
				std::cout << it->first;
				if (it->second > 1)
					std::cout << '^' << it->second;
			}
			return os;
		}
	};

	Primes_t() : lastChecked_(1) {} 

	void ensure(size_type n) {
		while (primes_.size() <= n) {
			while (!checkNextPrime())
				continue;
		}
	}

	void sieve(T n) {
		if (n <= lastChecked_)
			return;
		++n;
		const T sqn = static_cast<T>(std::sqrt(n)); 
		typedef std::vector<bool> BitVec;
		BitVec isprime(n, true);
		for (BitVec::size_type i = 2; i <= sqn; ++i) {
			if (!isprime[i])
				continue;
			for (BitVec::size_type j = i + i; j < n; j += i)
				isprime[j] = false;
		}
		while (++lastChecked_ < n) {
			if (isprime[lastChecked_]) {
				primes_.push_back(lastChecked_);
			}
		}
		--lastChecked_;
	}

	void checkTill(T n) {
		while (lastChecked_ < n)
			checkNextPrime();
	}

	bool check(T n) {
		if (lastChecked_ < n) {
			checkTill(n);
			return primes_.back() == n;
		}
		return std::binary_search(primes_.begin(), primes_.end(), n);
	}


	bool isPrime(T n) {
		if (n <= lastChecked_)
			return std::binary_search(primes_.begin(), primes_.end(), n);
		if (n % 2 == 0)
			return n == 2;
		const T sqn = static_cast<T>(std::sqrt(n)); 
		checkTill(sqn + 1);
		const_iterator pend = primes_.end();
		for (const_iterator i = primes_.begin(); i < pend && *i <= sqn; ++i)
			if (n % (*i) == 0)
				return false;
		return n > 1;
	}

	T operator[](size_type n) {
		ensure(n);
		return primes_[n];
	}
	
	const_iterator begin() const {
		return primes_.begin();
	}

	const_iterator end() const {
		return primes_.end();
	}

	const_iterator nth(size_type n) {
		ensure(n);
		return begin() + n;
	}

	size_type size() const { return primes_.size(); }

	T sigma2(T n) {
		const T sqn = static_cast<T>(std::sqrt(n)); 
		checkTill(sqn + 1);
		T res = 1;
		if (n == 1)
			return res;
		const const_iterator pend = end();
		for (const_iterator it = begin(); it != pend; ++it) {
			const T p = *it;
			if (p > sqn)
				break;
			if (n % p != 0)
				continue;
			const T p2 = p * p;
			T z = p2;
			T s = 1;
			while (n % p == 0) {
				s += z;
				z *= p2;
				n /= p;
				if (n == 1)
					return res * s;
			}
			res *= s;
		}
		return res * (1 + n * n);
	}

	Factors factor(T n) {
		const T sqn = static_cast<T>(std::sqrt(n)); 
		checkTill(sqn + 1);
		Factors res;
		if (n == 1)
			return res;
		const const_iterator pend = end();
		for (const_iterator it = begin(); it != pend; ++it) {
			const T p = *it;
			if (p > sqn)
				break;
			while (n % p == 0) {
				++res[p];
				n /= p;
				if (n == 1)
					return res;
			}
		}
		++res[n];
		return res;
	}

	T sumDivisors(T n) {
		T res = 1;
		int pi = 0;
		for (T k = (*this)[pi++]; k*k <= n; k = (*this)[pi++]) {
			T p = 1;
			while (n % k == 0) {
				p = p * k + 1;
				n /= k;
			}
			res *= p;
		}
		if (n > 1)
			res *= n + 1;
		return res;
	}

	T sumProperDivisors(T n) {
		return sumDivisors(n) - n;
	}

};

template<typename T>
bool isPrime(T p) {
	if (p % 2 == 0)
		return p == 2;
	for (T k = 3; k * k <= p; k += 2)
		if (p % k == 0)
			return false;
	return p > 1;
}

typedef Primes_t<> Primes;

#endif
