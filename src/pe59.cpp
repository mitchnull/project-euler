#include <iostream>
#include <sstream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <vector>
#include <cctype>

namespace {
	typedef boost::uint64_t Number;
	typedef std::vector<int> CipherText;

	bool accept(int ch) {
//		return std::isprint(ch);
		if (std::isalnum(ch) || std::isspace(ch))
			return true;
#if 0
		switch (ch) {
			case '.':
			case ',':
			case '!':
			case '?':
			case '"': 
			case '\'': 
				return true;
		}
#endif
		return false;
	}

	bool reject(int ch) {
		return !std::isprint(ch);
	}

	int sum(const std::string &str) {
		int res = 0;
		for (std::string::const_iterator i = str.begin(); i != str.end(); ++i)
			res += *i;
		return res;
	}

	std::string decrypt(std::string key, const CipherText &ct, const int pc) {
		std::string::size_type ki = 0;
		std::string::size_type ks = key.size();
		std::stringstream res;
		unsigned int gc = 0;
		for (CipherText::const_iterator i = ct.begin(); i != ct.end(); ++i) {
			int ch = (*i) ^ key[ki];
			if (reject(ch))
				return "";
			if (accept(ch))
				++gc;
			res << static_cast<char>(ch);
			++ki;
			ki %= ks;
		}
		if (gc < ct.size() * pc / 100)
			return "";
		return res.str();
	}

	std::string &inc(std::string &key) {
		for (int i = key.size() - 1; i >= 0; --i) {
			if (++key[i] > 'z') {
				key[i] = 'a';
				continue;
			}
			break;
		}
		return key;
	}
}

int main(int argc, const char **argv) {

	int n = 95;
	if (argc > 1)
		n = boost::lexical_cast<int>(argv[1]);

	int ch;
	char dummy;
	CipherText ct;
	while(std::cin >> ch) {
		ct.push_back(ch);
		if (!(std::cin >> dummy))
			break;
	}
	std::string key = "aaa";
	do {
		std::string pt = decrypt(key, ct, n);
		if (pt.size() > 0)
			std::cout << key << ": " << pt << ": " << sum(pt) << std::endl;
	} while (inc(key) != "aaa");
	return 0;
}
