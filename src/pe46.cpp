#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include <vector>
#include "primes.hpp"
#include "math.hpp"


namespace {
	typedef boost::uint64_t Number;

	Primes_t<Number> primes;

	bool check(Number n) {
		Primes::const_iterator pend = primes.end();
		for (Primes::const_iterator pi = primes.begin(); pi != pend && *pi < n; ++pi) {

			const Number r = n - *pi;
			if (r % 2 != 0)
				continue;
			if (isSquare(r / 2)) {
				std::cout << n << " = " << *pi << " + 2 * " << (r / 2) << std::endl;
				return true;
			}
		}
		return false;
	}
}

int
main(int argc, const char **argv) {
	for (Number i = 9; ; i += 2) {
		if (primes.check(i))
			continue;
		if (!check(i)) {
			std::cout << i << std::endl;
			return 0;
		}
	}
	return 0;
}
