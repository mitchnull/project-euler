#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include <map>
#include <vector>
#include "primes.hpp"
#include "math.hpp"


namespace {
	typedef boost::uint64_t Number;

	Primes_t<Number> primes;
	typedef std::vector<Primes::Factors> FactVec;
	FactVec fv;
	Number n;

	bool check() {
		std::set<Primes::Factors::value_type> allfact;
		for (FactVec::const_iterator i = fv.begin(); i != fv.end(); ++i) {
			allfact.insert(i->begin(), i->end());
		}
		return allfact.size() == fv.size() * fv.size();
	}
}

int
main(int argc, const char **argv) {
	n = 4;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Number i;
	Number cc = 0;
	for (i = 0; i < n; ++i) {
		fv.push_back(primes.factor(i));
		if (fv.back().size() == n)
			++cc;
		else
			cc = 0;
	}
	while (true) {
		++i;
		const Primes::Factors &f = fv[i % n] = primes.factor(i);
		if (f.size() == n) {
			if (++cc >= n && check()) {
				std::cout << i - n + 1 << std::endl;
				return 0;
			}
		}
		else {
			cc = 0;
		}
	}
	return 0;
}
