#include <iostream>
#include <sstream>
#include "primes.hpp"

int
main(int argc, const char **argv) {

	Primes::value_type n = 2000000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Primes primes;
	Primes::size_type i = 0;
	boost::uint64_t sum = 0;
	Primes::value_type p;
	while ((p = primes[i++]) < n) {
		sum += p;
	}

	std::cout << "SPB(" << n << ") = " << sum << std::endl;
	return 0;
}
