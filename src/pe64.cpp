#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <boost/rational.hpp>
#include "cfract.hpp"

namespace {

	typedef boost::uint64_t Number;
	typedef CFract_t<Number> CF;

}

int main(int argc, const char **argv) {

	Number n = 10000;
	int res = 0;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}

	for (Number i = 1; i <= n; ++i) {
		CF cf(CF::squareOf(i));
		if (cf.period() && (cf.period() % 2) == 1)
			++res;
#if 0
		double exp = cf.expand<double>();
		double sqi = std::sqrt(i);
		std::cout << "sqrt(" << i << ") = " << cf << " = " << exp << " ERR: " << (sqi - exp) << std::endl;
#endif
	}
	std::cout << res << std::endl;
	return 0;
}
