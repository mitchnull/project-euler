#include <iostream>
#include <algorithm>
#include <boost/cstdint.hpp>
#include <boost/lexical_cast.hpp>
#include <set>
#include "primes.hpp"


namespace {
	typedef boost::uint64_t Number;

	Primes_t<Number> primes;

	Number check(Number p) {
		std::set<Number> perms;
		std::string ps = boost::lexical_cast<std::string>(p);
		while (std::next_permutation(ps.begin(), ps.end())) {
			const Number pp = boost::lexical_cast<Number>(ps);
			perms.insert(pp);
		}
		for (std::set<Number>::const_iterator i = perms.begin(); i != perms.end(); ++i) {
			if (!primes.check(*i))
				continue;
			const Number d = *i - p;
			const Number ppp = *i + d;
			if (perms.find(ppp) != perms.end() && primes.check(ppp))
				return d;
		}
		return 0;
	}
}

int
main(int argc, const char **argv) {
	primes.check(999);
	Primes::size_type i = primes.size() + 1;
	Number d;
	for (Number p = primes[i]; p < 10000; p = primes[++i]) {
		if ((d = check(p)))
			std::cout << p << (p+d) << (p+d+d) << std::endl;
	}
	return 0;
}
