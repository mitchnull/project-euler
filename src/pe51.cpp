#include <iostream>
#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include "primes.hpp"
#include "math.hpp"
#include <vector>

namespace {
	typedef boost::uint64_t Number;
	typedef Primes_t<Number> P;
	typedef std::vector<bool> BitVec;

	P primes;

	Number replace(std::string &ps, const BitVec &v, int r) {
		for (BitVec::size_type i = 0; i < v.size(); ++i) {
			if (v[i]) {
				ps[i] = '0' + r;
			}
		}
		return boost::lexical_cast<Number>(ps);
	}

	void print(std::string &ps, const BitVec &v) {
		for (int r = 9; r >= 0; --r) {
			Number p = replace(ps, v, r);
			if (primes.check(p))
				std::cout << p << std::endl;
		}
	}
}

int main(int argc, const char **argv) {

	int n = 8;
	if (argc > 1)
		n = boost::lexical_cast<int>(argv[1]);

	primes.sieve(100000);

	for (P::size_type i = 0; ; ++i) {
		Number p = primes[i];
		int nd = numDigits(p) - 1;
		if (nd < 2)
			continue;
		Number min = pow(10, nd);
		BitVec v(nd);
		v[nd - 1] = true;
		for (int i = 2; i < nd; ++i) {
			v[nd - i] = true;
			do {
				int cp = 0;
				std::string ps = boost::lexical_cast<std::string>(p);
				for (int r = 0; r < 10; ++r) {
					const Number pr = replace(ps, v, r);
					if (pr >= min && primes.check(pr)) {
						if (++cp >= n) {
							print(ps, v);
							return 0;
						}
					}
				}
			} while (std::next_permutation(v.begin(), v.end()));
		}
	}
	return 0;
}
