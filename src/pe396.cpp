#include <iostream>
#include <boost/lexical_cast.hpp>
#include "magicnumber.hpp"


int main(int argc, const char **argv) {
    MagicNumber<> mn(6, 2);
    std::cout << mn << "\n";
    std::cout << (mn.get<uint64_t>(3) - 1) << "\n";
    return 0;
}
