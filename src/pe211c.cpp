#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <gmpxx.h>
#include "primes.hpp"
#include "math.hpp"

namespace {
	typedef boost::uint64_t Number;
}

int main(int argc, const char **argv) {

	Number n = 64000000LL;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}

	std::vector<Number> v(n);
	for (Number i = 2; i < n; ++i) {
		const Number i2 = i * i;
		for (Number j = i; j < n; j += i)
			v[j] += i2;
	}
	Number res = 0;

	std::cout << "starting search" << std::endl;
	for (Number i = 1; i < n; ++i) {
		if (isSquare(v[i] + 1)) {
			res += i;
		}
	}
	std::cout << res << std::endl;
	return 0;
}
