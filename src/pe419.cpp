#include <iostream>
#include <vector>

using Number = long;
using Digit = char;
class SayNum {
    std::string digits;

    void appendDigits(Number number) {
#if 0
        digits.append(std::to_string(number));
#else
        while (number >= 10) {
            Number d = number / 10;
            number %= 10;
            digits.push_back(static_cast<Digit>(d));
        }
        digits.push_back(static_cast<Digit>(number));
#endif
    }

public:
    SayNum() {
    }

    SayNum(Number num) {
        appendDigits(num);
    }

    SayNum next() const {
        SayNum res;
        for (auto i = digits.cbegin(), end = digits.cend(); i != end; ) {
            Digit d = *(i++);
            Number n = 1;
            while (i != end && *i == d) {
                ++n;
                ++i;
            }
            res.appendDigits(n);
            res.digits.push_back(d);
        }
        return res;
    }
    friend 
    std::ostream& operator<<(std::ostream& os, const SayNum& sn) {
        for (auto c : sn.digits) {
            os << static_cast<char>('0' + c);
        }
        return os;
    }
};

int main(int argc, const char **argv) {
    Number n = 10;
    if (argc > 1) {
        n = std::stoi(argv[1]);
    }
    SayNum sn(1);
    while (n--) {
        std::cout << sn << "\n";
        sn = sn.next();
    }
    return 0;
}
