#include <iostream>
#include <sstream>
#include <boost/cstdint.hpp>
#include <boost/rational.hpp>
#include <boost/lexical_cast.hpp>
#include <gmpxx.h>
#include "math.hpp"

typedef mpz_class Number;
typedef boost::rational<Number> Rat;

namespace {

	bool check(const Rat &r) {
		return (numDigits(r.numerator()) > numDigits(r.denominator()));
	}
}

int main(int argc, const char **argv) {
	int n = 1000;
	if (argc > 1) {
		n = boost::lexical_cast<int>(argv[1]);
	}
	int res = 0;
	Rat v(3,2);
	for (int i = 1; i < n; ++i) {
		if (check(v))
			++res;
		v = 1 + 1 / (1 + v);
	}
	std::cout << res << std::endl;
	return 0;
}
