#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include "primes.hpp"
#include "math.hpp"

typedef boost::uint64_t Number;

namespace {
	Primes_t<Number> primes;

	template <typename T>
	T rotate(const T &n) {
		T big = (n % 10) * pow(10, numDigits(n)-1);
		return big + n / 10;
	}
	bool isCircularPrime(const Number &p) {
		for (Number pp = rotate(p); pp != p; pp = rotate(pp))
			if (!primes.check(pp))
				return false;
		return true;
	}

}

int
main(int argc, const char **argv) {
	Number n = 1000000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	std::set<Number> circularPrimes;
	primes.check(n); // pre-calculate primes
	for (int i = 0; primes[i] < n; ++i) {
		if (isCircularPrime(primes[i])) {
			circularPrimes.insert(primes[i]);
			std::cout << primes[i] << std::endl;
		}
	}
	std::cout << "NumCircular(" << n << ") = " << circularPrimes.size() << std::endl;
	return 0;
}
