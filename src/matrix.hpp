#ifndef PE_MATRIX_HPP
#define PE_MATRIX_HPP

#include <vector>
#include <iostream>

template<typename T>
class Matrix {
private:
	typedef typename std::vector<T> PV;
public:
	typedef typename PV::size_type size_type;
	typedef T value_type;
	typedef typename PV::iterator iterator;
	typedef typename PV::const_iterator const_iterator;

private:
	size_type rows_, cols_;
	PV v_;

public:

	Matrix(size_type rows, size_type cols) : rows_(rows), cols_(cols), v_(rows * cols) {}

	T &operator()(size_type i, size_type j) {
		return v_[i*cols_ + j];
	}

	const T &operator()(size_type i, size_type j) const {
		return v_[i*cols_ + j];
	}

	const_iterator begin() const {
		return v_.begin();
	}

	const_iterator end() const {
		return v_.end();
	}

	iterator begin() {
		return v_.begin();
	}

	iterator end() {
		return v_.end();
	}

	size_type rows() const { return rows_; }
	size_type cols() const { return cols_; }

//	friend std::istream &operator>>(std::istream &is, Matrix<T> &m);
//	friend std::ostream &operator<<(std::ostream &os, const Matrix<T> &m);

};

template<typename T>
std::istream &operator>>(std::istream &is, Matrix<T> &m) {
	for (typename Matrix<T>::size_type i = 0; i < m.rows(); ++i) {
		for (typename Matrix<T>::size_type j = 0; j < m.cols(); ++j) {
			is >> m(i, j);
		}
	}
	return is;
}

template<typename T>
std::ostream &operator<<(std::ostream &os, const Matrix<T> &m) {
	for (typename Matrix<T>::size_type i = 0; i < m.rows(); ++i) {
		bool isFirst = true;
		for (typename Matrix<T>::size_type j = 0; j < m.cols(); ++j) {
			if (isFirst) {
				isFirst = false;
			}
			else {
				os << ' ';
			}
			os << m(i, j);
		}
		os << std::endl;
	}
	return os;
}

#endif
