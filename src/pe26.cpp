#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <vector>

namespace {
	typedef int Number;

	Number plen(Number n) {
		std::vector<Number> rs;
		Number t = 1;
//		std::cout << "1/" << n << "=";
		while (t) {
			int d = t / n;
//			std::cout << d;
			t -= d * n;
			std::vector<Number>::const_iterator prev = std::find(rs.begin(), rs.end(), t);
			if (prev != rs.end()) {
//				std::cout << "[" << (rs.end() - prev) << "]" << std::endl;
				return rs.end() - prev;
			}
			rs.push_back(t);
			t *= 10;
		}
//		std::cout << std::endl;
		return 0;
	}
}

int main(int argc, const char **argv) {

	Number n = 1000;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}

	Number resl = 0;
	Number res = 0;

	for (Number i = 1; i < n; ++i) {
		const Number il = plen(i);
		if (il > resl) {
			res = i;
			resl = il;
		}
	}

	std::cout << res << " [" << resl << "]" << std::endl;
	return 0;
}
