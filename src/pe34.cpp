#include <iostream>
#include <sstream>
#include <boost/cstdint.hpp>
#include <boost/rational.hpp>
#include "math.hpp"

typedef boost::uint64_t Number;
typedef boost::rational<Number> Rat;

namespace {
	template <typename T>
	T sumDigFact(T n) {
		static int Facts[10] = {
			factorial(0),
			factorial(1),
			factorial(2),
			factorial(3),
			factorial(4),
			factorial(5),
			factorial(6),
			factorial(7),
			factorial(8),
			factorial(9)
		};
		if (n == 0)
			return 1;
		T res = 0;
		while (n != 0) {
			int d = n % 10;
			res += Facts[d];
			n /= 10;
		}
		return res;
	}
}

int main(int argc, const char **argv) {
	Number res = 0;
	const Number F9 = factorial(9);
	for (Number i = 10; F9 * numDigits(i) >= i; ++i) {
		if (i == sumDigFact(i)) {
			std::cout << i << std::endl;
			res += i;
		}
	}
	std::cout << "SUM = " << res << std::endl;
	return 0;
}
