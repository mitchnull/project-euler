#ifndef PE_FIBONACCI_HPP
#define PE_FIBONACCI_HPP

#include <boost/cstdint.hpp>
#include <iterator>

template<typename T = boost::uint64_t>
class Fibonacci_t : public std::iterator<std::forward_iterator_tag, T> {
	T n0_, n1_;

public:
	Fibonacci_t() : n0_(0), n1_(1) {}
	bool operator==(const Fibonacci_t<T> &other) {
		return n0_ == other.n0_ && n1_ == other.n1_;
	}
	bool operator!=(const Fibonacci_t<T> &other) {
		return !operator==(other);
	}
	Fibonacci_t<T> operator++(int) {
		Fibonacci_t<T> tmp(*this);
		++(*this);
		return tmp;
	}
	Fibonacci_t<T> &operator++() {
		T tmp = n1_;
		n1_ += n0_;
		n0_ = tmp;
		return *this;
	}
	const T &operator*() {
		return n1_;
	}
	const T *operator->() {
		return &n1_;
	}
};

typedef Fibonacci_t<> Fibonacci;

#endif
