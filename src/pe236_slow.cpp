#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <boost/rational.hpp>
#include <cctype>
#include <vector>

namespace {
	typedef boost::uint64_t Number;
	typedef boost::rational<Number> Rat;
	typedef std::vector<Number> NumVec;
	typedef std::pair<Number, Number> NumPair;
	typedef std::vector<std::vector<NumPair> > NumPairsVec;

	enum { NumProds = 5 };

	Number Prods[2][NumProds] = {
		{ 5248, 1312, 2624, 5760, 3936 },
		{ 640, 1888, 3776, 3776,  5664 }
	};

	Number Sum[2];

	Rat pps(std::size_t suppl, std::size_t prod, Number spoil) {
		return Rat(spoil, Prods[suppl][prod]);
	}

	void normalize() {
		Number limit = Prods[0][0];
		for (std::size_t i = 0; i < 2; ++i) {
			for (std::size_t j = 0; j < NumProds; ++j) {
				if (Prods[i][j] < limit)
					limit = Prods[i][j];
			}
		}
		for (Number k = 2; k <= limit; ) {
			for (std::size_t i = 0; i < 2; ++i) {
				for (std::size_t j = 0; j < NumProds; ++j) {
					if (Prods[i][j] % k != 0)
						goto nextk;
				}
			}
			for (std::size_t i = 0; i < 2; ++i) {
				for (std::size_t j = 0; j < NumProds; ++j) {
					Prods[i][j] /= k;
				}
			}
			continue;
nextk:
			if (k == 2)
				++k;
			else 
				k += 2;
		}
		for (std::size_t i = 0; i < 2; ++i) {
			for (std::size_t j = 0; j < NumProds; ++j) {
				std::cout << Prods[i][j] << " ";
			}
			std::cout << std::endl;
		}
	}

	template<typename It>
	Number sum(It begin, It end) {
		Number res = 0;
		while (begin != end)
			res += *(begin++);
		return res;
	}

	bool inc(const NumPairsVec &vp, NumVec &selection) {
		for (std::size_t i = 1; i < NumProds; ++i) {
			if (++selection[i] < vp[i].size())
				return true;
			selection[i] = 0;
		}
		return false;
	}

	bool init(NumPairsVec &vp, const Rat &m) {
		for (std::size_t i = 1; i < NumProds; ++i) {
			for (Number j = 1; j <= Prods[0][i]; ++j) {
				Rat ps = m * (j * Prods[1][i]) / Prods[0][i];
				if (ps.denominator() != 1)
					continue;
				vp[i].push_back(std::make_pair(j, ps.numerator()));
			}
			if (vp[i].empty())
				return false;
		}
		return true;
	}

	Rat osRatio(const NumPairsVec &vp, const NumVec &selection) {
		Number os0 = 0;
		Number os1 = 0;
		for (std::size_t i = 0; i < NumProds; ++i) {
//			std::cout << vp[i][selection[i]].first << "/" << Prods[0][i] << ", " << vp[i][selection[i]].second << "/" << Prods[1][i] << std::endl;
			os0 += vp[i][selection[i]].first;
			os1 += vp[i][selection[i]].second;
		}
//		std::cout << os0 << "/" << Sum[0] << ", " << os1 << "/" << Sum[1] << std::endl;
		const Rat or0(os0, Sum[0]);
		const Rat or1(os1, Sum[1]);
		const Rat res = or0 / or1;
//		std::cout << "m: " <<  res << std::endl;
		return res;
	}

	bool check(Number sp00, Number sp10, const Rat &m) {
		NumPairsVec validPairs(NumProds);
		validPairs[0].push_back(std::make_pair(sp00, sp10));
		if (!init(validPairs, m))
			return false;
		NumVec selection(NumProds);
		do {
			if (osRatio(validPairs, selection) == m)
				return true;
		} while (inc(validPairs, selection));
		return false;
	}
}

int main(int argc, const char **argv) {

//	normalize();
	Sum[0] = sum(&Prods[0][0], &Prods[0][NumProds]);
	Sum[1] = sum(&Prods[1][0], &Prods[1][NumProds]);

	Rat res(0);
	for (Number i = 1; i <= Prods[0][0]; ++i) {
		Rat pp00 = pps(0, 0, i);
		for (Number j = 1; j <= Prods[1][0]; ++j) {
			Rat pp10 = pps(1, 0, j);
			if (pp10 < pp00) // TODO: put it in loop start/stop condition
				continue;
			Rat m = pp10 / pp00;
			if (m < res) // TODO: put it in loop start/stop condition
				continue;
			std::cout << i << ", " << j << ": " << m << std::endl;
			if (check(i, j, m) && m > res) {
				res = m;
				std::cout << res << std::endl;
			}
		}
	}
	std::cout << res << std::endl;
	return 0;
}
