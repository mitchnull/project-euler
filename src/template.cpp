#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>

namespace {
	typedef boost::uint64_t Number;
}

int main(int argc, const char **argv) {

	Number n = 1000;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}

	std::cout << n << std::endl;
	return 0;
}
