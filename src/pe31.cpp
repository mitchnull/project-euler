#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include "math.hpp"
#include <vector>

// backtrack
typedef int Number;

namespace {
	Number n = 200;
	const Number DefaultCoins[] = { 1, 2, 5, 10, 20, 50, 100, 200 };

	std::vector<Number> coins;
	std::vector<Number> nums;
	Number currSum = 0;

	typedef std::vector<Number>::size_type size_type;
	
	template<typename T, size_t n>
		char (& as_char_array(const T (&)[n]) )[n];

	void print() {
		for (size_type i = 0; i < coins.size(); ++i) {
			if (nums[i] == 0)
				continue;
			std::cout << ' ' << nums[i] << "x(" << coins[i] << ")";
		}
		std::cout << std::endl;
	}
	
	Number sum() {
		Number res = 0;
		for (size_type i = 0; i < coins.size(); ++i) {
			res += coins[i] * nums[i];
		}
		return res;
	}

	bool next() {
		size_type pos = 0;
		size_type nc = coins.size();
		while (pos < nc) {
			++nums[pos];
			currSum += coins[pos];
			if (currSum <= n)
				return true;
			// overflow, step p
			for (size_type i = 0; i <= pos; ++i)
				nums[i] = 0;
			currSum = sum();
			++pos;
		}
		return false;
	}

}

int
main(int argc, const char **argv) {

	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	if (argc > 2) {
		for (int i = 2; i < argc; ++i) {
			Number c;
			std::stringstream s(argv[i]);
			s >> c;
			coins.push_back(c);
		}
	}
	else {
		for (const Number *p = DefaultCoins; p < DefaultCoins + sizeof(as_char_array(DefaultCoins)); ++p) {
			if (*p == 0) {
				std::cerr << "coin cannot be 0" << std::endl;
				return -1;
			}
			coins.push_back(*p);
		}
	}
	std::cout << "Target: " << n << std::endl;
	std::cout << "Coins:";
	for (size_type i = 0; i < coins.size(); ++i) {
		std::cout << ' ' << coins[i] << "";
	}
	std::cout << std::endl;

	Number count = 0;
	nums.resize(coins.size());
	if (n == 0) // degenerate case
		++count;
	while (next()) {
		if (currSum == n) {
			++count;
//			print();
		}
	}

	std::cout << "COUNT = " << count << std::endl;
	return 0;
}
