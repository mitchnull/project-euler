#include <iostream>
#include <sstream>
#include "primes.hpp"

int
main(int argc, const char **argv) {

	Primes::value_type n = 100;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Primes primes;
	Primes::Factors factors = primes.factor(n);

	std::cout << "Factors(" << n << ") =";
	for (Primes::Factors::const_iterator i = factors.begin(); i != factors.end(); ++i) {
		std::cout << ' ' << i->first;
		if (i->second > 1)
			std::cout << '^' << i->second;
	}
	std::cout << std::endl;
	return 0;
}
