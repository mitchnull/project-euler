#include <iostream>
#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <map>
#include "math.hpp"
#include <cctype>

namespace {
	typedef mpz_class Number;
	typedef std::pair<Number, std::size_t> Rec;
	typedef std::map<std::string, Rec> Cubes;
	typedef std::map<Number, std::string> Candidates;
	
	Cubes cubes;
	Candidates candidates;
	std::size_t len;
	std::size_t n = 5;

	Number inc(Number i) {
		Number i3 = pow(i, 3);
		std::string i3s = boost::lexical_cast<std::string>(i3);
		std::sort(i3s.begin(), i3s.end(), std::greater<char>());
//		std::cout << i3 << " -> " << i3s << std::endl;
		if (i3s.size() > len) {
			while (len++ < i3s.size()) (std::cout << '.').flush();

			for (Candidates::const_iterator i = candidates.begin(); i != candidates.end(); ++i) {
				if (cubes[i->second].second == n) {
					return i->first;
				}
			}
			cubes.clear();
			candidates.clear();
		}
		Rec &r = cubes[i3s];
		if (!r.second)
			r.first = i3;
		if (++r.second == n) {
			candidates[r.first] = i3s;
		}
		return 0;
	}
}

int main(int argc, const char **argv) {


	if (argc > 1) {
		n = boost::lexical_cast<std::size_t>(argv[1]);
	}

	Number res = 0;
	for (Number i = 0; (res = inc(i)) == 0; ++i)
		continue;

	std::cout << std::endl;
	std::cout << res << std::endl;
	return 0;
}
