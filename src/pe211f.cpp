#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <gmpxx.h>
#include "primes.hpp"
#include "math.hpp"

namespace {
	typedef boost::uint64_t Number;
	typedef Primes_t<Number> P;

	P primes;

	bool check(Number n) {
		Number sig2 = primes.sigma2(n);
		if (isSquare(sig2)) {
			std::cout << n << " -> " << sig2 << std::endl;
			return true;
		}
		return false;
	}
}

int main(int argc, const char **argv) {

	Number n = 64000000LL;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}

	primes.sieve(n);
	std::cout << primes.size() << std::endl;
	if (true) return 0;
	Number res = 0;

	for (Number i = 1; i < n; ++i) {
		if (check(i)) {
			res += i;
		}
	}
	std::cout << res << std::endl;
	return 0;
}
