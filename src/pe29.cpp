#include <iostream>
#include <algorithm>
#include <sstream>
#include <set>
#include "math.hpp"
#include <gmpxx.h>

typedef mpz_class Number;

int
main(int argc, const char **argv) {

	Number n = 100;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	std::set<Number> dset;
	for (Number a = 2; a <= n; ++a) {
		for (Number b = 2; b <= n; ++b) {
			dset.insert(pow(a, b));
		}
	}

	std::cout << "DS(a^b, 2<= a, b <= " << n << ") = " << dset.size() << std::endl;
	return 0;
}
