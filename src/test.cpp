#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <algorithm>

namespace {
	typedef boost::uint64_t Number;

	class Repl {
		char from_;
		char to_;
	public:
		Repl(char from, char to) : from_(from), to_(to) {}

		char operator()(const char &c) {
			if (c == from_)
				return to_;
			return c;
		}
	};
}

int main(int argc, const char **argv) {

	Number n = 1000;

	std::string test = "this-is-a-test";

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}

	std::transform(test.begin(), test.end(), test.begin(), Repl('-', '*'));
	std::cout << test << std::endl;
	return 0;
}
