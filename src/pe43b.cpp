#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>


namespace {
	typedef boost::uint64_t Number;

	class Perm {
		int d4_, d6_;
		int d_[8];

	public:
		Perm(int d4, int d6) : d4_(d4), d6_(d6) {
			d_[0] = 1;
			int cd = 0;
			for (int i = 1; i < 8; ++i) {
				while (cd == 1 || cd == d4 || cd == d6)
					++cd;
				d_[i] = cd++;
			}
		}

		bool next() {
			return std::next_permutation(&d_[0], &d_[8]);
		}

		int d(int i) const {
			switch (i) {
				case 1: case 2: case 3:
					return d_[i - 1];
				case 4:
					return d4_;
				case 5:
					return d_[i - 2];
				case 6:
					return d6_;
				default:
					return d_[i-3];
			}
		}

		Number value() {
			Number res = 0;
			for (int i = 1; i <= 10; ++i)
				res = 10 * res + d(i);
			return res;
		}

		bool check() {
			if ((d(3) + d(4) + d(5)) % 3 != 0)
				return false;
			if (value(d(5), d(6), d(7)) % 7 != 0)
				return false;
			if (value(d(6), d(7), d(8)) % 11 != 0)
				return false;
			if (value(d(7), d(8), d(9)) % 13 != 0)
				return false;
			if (value(d(8), d(9), d(10)) % 17 != 0)
				return false;
			return true;
		}

		static int value(int a, int b, int c) {
			return 100*a + 10*b + c;
		}
	};

	Number sum(int d4, int d6) {
		Perm p(d4, d6);
//		std::cout << "sum(" << d4 << ", " << d6 << "): " << p.value() << std::endl;
		Number res = 0;
		do {
			if (p.check()) {
				std::cout << p.value() << std::endl;
				res += p.value();
			}
		} while (p.next());
		return res;
	}
}

int
main(int argc, const char **argv) {
	int numRuns = 1;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> numRuns;
	}
	while (numRuns--) {
	Number res = 0;
	for (int d4 = 0; d4 < 10; d4 += 2) {
		for (int d6 = 0; d6 < 10; d6 += 5) {
			if (d4 == d6)
				continue;
			res += sum(d4, d6);
		}
	}
	std::cout << "SUM = " << res << std::endl;
	}
	return 0;
}
