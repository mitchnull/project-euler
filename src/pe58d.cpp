#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include "primes.hpp"

namespace {
//	typedef boost::uint64_t Number;
	typedef unsigned int Number;

	Number c(Number d, int n) {
		return (d-2) * (d-2) + n*(d - 1);
	}
}

int main(int argc, const char **argv) {

	Number n = 26241;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}

	Primes_t<Number> primes;
	primes.sieve(n);
	std::cout << "sieve done" << std::endl;

	Number numPrimes = 0;
	Number numDiag = 1;
	Number cpc = 0;
	Number j = 1;
	for (Number d = 2; ; d += 2) {
		for (int i = 0; i < 4; ++i) {
			j += d;
			if (primes.isPrime(j))
				++numPrimes;
			++numDiag;
		}
		const Number pc = numPrimes * 100 / numDiag;
		if (pc != cpc) {
			std::cout << (d+1) << ": " << numPrimes << "/" << numDiag << " = " << pc << "; " << j << std::endl;
			cpc = pc;
		}
		if (pc < 10) {
			std::cout << (d+1) << std::endl;
			break;
		}
	}
	std::cout << primes[primes.size() - 1] << std::endl;
	return 0;
}
