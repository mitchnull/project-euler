#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include "math.hpp"

typedef boost::uint64_t Number;

namespace {

	int
	numSolutions(Number p) {
		int res = 0;
		for (Number a = 1; a < p / 3; ++a) {
			for (Number b = a; b < p - a ; ++b) {
				const Number c = (p - a - b);
				if (c < b)
					break;
				if (a*a + b*b == c*c) {
					if (res == 0)
						std::cout << p << ":";
					std::cout << " (" << a << ", " << b << ", " << c << ")";
					++res;
				}
			}
		}
		if (res != 0)
			std::cout << " = " << res << std::endl;
		return res;
	}
}

int
main(int argc, const char **argv) {
	Number n = 1000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Number maxp = 0;
	Number maxs = 0;
	for (Number p = n; p > 0; --p) {
		const Number ns = numSolutions(p);
		if (ns > maxs) {
			maxs = ns;
			maxp = p;
		}
	}
	std::cout << maxp << std::endl;
	return 0;
}
