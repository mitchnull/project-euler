#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include <vector>
#include "primes.hpp"
#include "math.hpp"


namespace {
	typedef boost::uint64_t Number;

	template <typename T>
	bool isPali0N(T number, int n = 9) {
		int bits = 1;
		while (number != 0) {
			const int d = 1 << (number % 10);
			if (bits & d)
				return false;
			bits |= d;
			number /= 10;
		}
		return bits == (1 << ++n) - 1;
	}

	int value(int a, int b, int c) {
		return 100*a + 10*b + c;
	}
	
	Number value(int d4, int d6, const int *d) {
		return
			1000000000LL * d[0] +
			100000000LL * d[1] +
			10000000LL * d[2] +
			1000000LL * d4 +
			100000LL * d[3] +
			10000LL * d6 +
			1000LL * d[4] +
			100LL * d[5] +
			10LL * d[6] +
			d[7];
	}

	bool check(int d4, int d6, const int *d) {
		// d1 = d[0]
		// d2 = d[1]
		// d3 = d[2]
		// d4 = d4
		// d5 = d[3]
		// d6 = d6
		// d7 = d[4]
		// d8 = d[5]
		// d9 = d[6]
		// d10 = d[7]
		if ((d[2] + d4 + d[3]) % 3 != 0)
			return false;
		if (value(d[3], d6, d[4]) % 7 != 0)
			return false;
		if (value(d6, d[4], d[5]) % 11 != 0)
			return false;
		if (value(d[4], d[5], d[6]) % 13 != 0)
			return false;
		if (value(d[5], d[6], d[7]) % 17 != 0)
			return false;
		return true;
	}

	Number sum(int d4, int d6) {
		static int d[8];
		Number res = 0;
		d[0] = 1;
		int cd = 0;
		for (int i = 1; i < 8; ++i) {
			while (cd == 1 || cd == d4 || cd == d6)
				++cd;
			d[i] = cd++;
		}
		do {
			if (check(d4, d6, d)) {
				std::cout << value(d4, d6, d)<< std::endl;
				res += value(d4, d6, d);
			}
		} while (std::next_permutation(&d[0], &d[8]));
		return res;
	}
}

int
main(int argc, const char **argv) {
	int numRuns = 1;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> numRuns;
	}
	while (numRuns--) {
	Number res = 0;
	for (int d4 = 0; d4 < 10; d4 += 2) {
		for (int d6 = 0; d6 < 10; d6 += 5) {
			res += sum(d4, d6);
		}
	}
	std::cout << "SUM = " << res << std::endl;
	}
	return 0;
}
