#include <iostream>
#include <sstream>
#include <gmpxx.h>
#include "fibonacci.hpp"

typedef mpz_class Number;

int
main(int argc, const char **argv) {

	int n = 1000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	std::string limitStr = "1";
	for (int i = 1; i < n; ++i)
		limitStr += '0';
	Number limit(limitStr);
	Fibonacci_t<Number> f;
	int k = 1;
	while ((*f) < limit) {
		++f;
		++k;
	}
	std::cout << "First Fibonacci with " << n << " digits: F(" << k << ") = " << *f << std::endl;
	return 0;
}
