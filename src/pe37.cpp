#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include "primes.hpp"
#include "math.hpp"

typedef boost::uint64_t Number;

namespace {
	Primes_t<Number> primes;

	bool isLRTPrime(const Number &p) {
		for (Number pp = truncRight(p); pp != 0; pp = truncRight(pp))
			if (!primes.check(pp))
				return false;
		for (Number pp = truncLeft(p); pp != 0; pp = truncLeft(pp))
			if (!primes.check(pp))
				return false;
		return true;
	}

}

int
main(int argc, const char **argv) {
	Number n = 11;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	int i = 0;
	while (primes[i] < 10)
		++i;
	Number sum = 0;
	for (Number count = 0; count < n; ++i) {
		if (isLRTPrime(primes[i])) {
			std::cout << primes[i] << std::endl;
			sum += primes[i];
			++count;
		}
	}
	std::cout << "SumLRT(" << n << ") = " << sum << std::endl;
	return 0;
}
