#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include <vector>
#include "math.hpp"
#include <cmath>
#include <limits>


namespace {
	typedef boost::uint64_t Number;

	Number penta(Number n) {
		return (n * (3*n - 1)) / 2;
	}

	bool isPenta(Number t) {
		Number n = static_cast<Number>(((1 +  std::sqrt(1 + 24 * t)) / 6) + 0.5);
		return penta(n) == t;
	}
}

int
main(int argc, const char **argv) {
	int n = 100;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	int ns = 2;
	if (argc > 2) {
		std::stringstream s(argv[2]);
		s >> ns;
	}

	for (int m = ns; m <= n; ++m) {
		for (int j = ns; j <= n; ++j) {
			for (int k = j + 1; k < j + m; ++k) {
				const Number pj = penta(j);
				const Number pk = penta(k);
				if (isPenta(pk + pj) && isPenta(pk - pj)) {
					std::cout << "|P(" << j << ") - P(" << k << ")| = " << (pk - pj) << std::endl;
					return 0;
				}
			}
		}
	}
	return 0;
}
