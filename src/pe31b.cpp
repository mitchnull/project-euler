#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include "math.hpp"
#include <vector>

// DP
typedef int Number;

namespace {
	typedef std::vector<Number>::size_type size_type;
	
	template<typename T, size_t n>
		char (& as_char_array(const T (&)[n]) )[n];

}

int
main(int argc, const char **argv) {

	Number n = 200;
	const Number DefaultCoins[] = { 1, 2, 5, 10, 20, 50, 100, 200 };

	std::vector<Number> coins;
	std::vector<Number> nums;

	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	if (argc > 2) {
		for (int i = 2; i < argc; ++i) {
			Number c;
			std::stringstream s(argv[i]);
			s >> c;
			coins.push_back(c);
		}
	}
	else {
		for (const Number *p = DefaultCoins; p < DefaultCoins + sizeof(as_char_array(DefaultCoins)); ++p) {
			if (*p == 0) {
				std::cerr << "coin cannot be 0" << std::endl;
				return -1;
			}
			coins.push_back(*p);
		}
	}
	std::sort(coins.begin(), coins.end());
	std::cout << "Target: " << n << std::endl;
	std::cout << "Coins:";
	for (size_type i = 0; i < coins.size(); ++i) {
		std::cout << ' ' << coins[i] << "";
	}
	std::cout << std::endl;

	nums.resize(n + 1);
	nums[0] = 1;
	for (std::vector<Number>::const_iterator j = coins.begin(); j != coins.end(); ++j) {
		const Number c = *j;
		for (Number i = 0; i <= n - c; ++i) {
			nums[i + c] += nums[i];
		}
	}
//	for (int i = 0; i <= n; ++i)
//		std::cout << "nums[" << i << "] = " << nums[i] << std::endl;

	std::cout << "COUNT = " << nums.back() << std::endl;
	return 0;
}
