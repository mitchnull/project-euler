#include <vector>
#include <algorithm>
#include <map>
#include <iostream>
#include <limits>

namespace {
	typedef std::vector<int> IntVec;
	typedef std::map<int, int> IntMap;
	typedef std::pair<int, int> IntPair;

	IntVec
	readVec(std::istream &in) {
		IntVec res;
		int tmp;
		while (in >> tmp) {
			res.push_back(tmp);
		}
		in.clear();
		in.get();
		return res;
	}
	
	int
	minValue(const IntMap &idxMap) {
		int res = std::numeric_limits<int>::max();
		for (IntMap::const_iterator i = idxMap.begin(); i != idxMap.end(); ++i) {
			if (i->second == -1)
				return -1;
			if (i->second < res)
				res = i->second;
		}
		return res;
	}

	IntPair
	smallestWindow(const IntVec &a, const IntVec &b) {
		IntPair res(-1, -1);
		IntMap lastIdx;
		for (IntVec::const_iterator i = b.begin(); i != b.end(); ++i) {
			lastIdx[*i] = -1;
		}
		int idx = 0;
		for (IntVec::const_iterator i = a.begin(); i != a.end(); ++i, ++idx) {
			const int &v = *i;
			if (lastIdx.find(v) == lastIdx.end())
				continue;
			lastIdx[v] = idx;
			int minIdx = minValue(lastIdx);
			if (minIdx > 0 && (res.first == -1 || idx - minIdx < res.second - res.first)) {
				res.first = minIdx;
				res.second = idx;
			}
		}
		return res;
	}
}

int
main(int argc, const char **argv) {
	IntVec a = readVec(std::cin);
	IntVec b = readVec(std::cin);
	std::pair<int, int> sw = smallestWindow(a, b);
	std::cout << sw.first << ", " << sw.second << std::endl;
	return 0;
}
