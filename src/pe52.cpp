#include <iostream>
#include <algorithm>
#include <boost/cstdint.hpp>
#include <boost/lexical_cast.hpp>


namespace {
	typedef boost::uint64_t Number;

	std::string sortedDigits(Number a) {
		std::string res = boost::lexical_cast<std::string>(a);
		std::sort(res.begin(), res.end());
		return res;
	}

	bool isSameDigits6(Number a) {
		std::string ad = sortedDigits(a);
		Number m = a;
		for (int i = 0; i < 5; ++i) {
			m += a;
			std::string md = sortedDigits(m);
			if (ad != md)
				return false;
		}
		return true;
	}
}

int
main(int argc, const char **argv) {
	Number res = 0;
	while (!isSameDigits6(++res)) {
		continue;
	}
	std::cout << res << std::endl;
	return 0;
}
