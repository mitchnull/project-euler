#include <iostream>
#include <sstream>
#include <cmath>
#include <boost/cstdint.hpp>
#include <gmpxx.h>

namespace {
	template <typename T>
	T pow(const T &a, int n) {
		T res = 1;
		for (int i = 0; i < n; ++i)
			res *= a;
		return res;
	}

	template <typename T>
	T sumDigits(T a) {
		T res = 0;
		while (a > 0) {
			res += (a % 10);
			a /= 10;
		}
		return res;
	}
}

int
main(int argc, const char **argv) {

	int n = 1000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	mpz_class pow2n = pow(mpz_class(2), n);
	std::cout << "2^" << n << ") = " << pow2n << std::endl;
	std::cout << "SumDigits(2^" << n << ") = " << sumDigits(pow2n) << std::endl;
	return 0;
}
