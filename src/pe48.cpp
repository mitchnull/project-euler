#include <iostream>
#include <sstream>
#include <gmpxx.h>

typedef mpz_class Number;

namespace {
	template <typename T>
	T pow(const T &base, const T &exp) {
		T res = 1;
		for (T i = 0; i < exp; ++i)
			res *= base;
		return res;
	}
}

int
main(int argc, const char **argv) {

	Number n = 1000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Number sum = 0;
	for (Number i = 1; i <= n; ++i)
		sum += pow(i, i);
	std::cout << "SUM(i^i)[" << n << "] = " << sum << std::endl;
	std::string sumStr = sum.get_str();
	if (sumStr.size() > 10) {
		std::cout << "Last 10 digits: " << sumStr.substr(sumStr.size() - 10) << std::endl;
	}
	return 0;
}
