#include <iostream>
#include <sstream>
#include <set>
#include <boost/cstdint.hpp>

namespace {

class Name {
	std::string str_;
	int score_;
public:
	Name() : score_(0) {}

	friend std::istream &operator>>(std::istream &is, Name &name) {
		name.str_.clear();
		name.score_ = 0;
		char ch;
		bool foundStart = false;
		while (is >> ch) {
			switch (ch) {
				case '"':
					if (foundStart)
						return is;
					else
						foundStart = true;
					break;
				case ',':
					break;
				default:
					name.str_ += ch;
					name.score_ += ch - 'A' + 1;
					break;
			}
		}
		return is;
	}

	int score() const { return score_; }
	std::string str() const { return str_; }

	bool operator<(const Name &other) const {
		return str_ < other.str_;
	}
};

	std::set<Name> names;
}

int
main(int argc, const char **argv) {

	boost::uint64_t res = 0;

	Name name;
	while (std::cin >> name) {
		names.insert(name);
	}
	int pos = 1;
	for (std::set<Name>::const_iterator i = names.begin(); i != names.end(); ++i) {
		std::cout << pos << ": " << i->str() << ": " << i->score() << std::endl;
		res += i->score() * pos;
		++pos;
	}
	std::cout << "SumScore: " << res << std::endl;
	return 0;
}
