#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>

typedef boost::uint64_t Number;

int main(int argc, const char **argv) {

	Number n = 1000;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}

	Number sum = 0;
	for (Number i = 0; i < n; ++i) {
		std::string s = boost::lexical_cast<std::string>(i);
		Number k = boost::lexical_cast<Number>(s);
		sum += k;
	}
	std::cout << sum << std::endl;
	return 0;
}
