#ifndef PE_FIGURATE_HPP
#define PE_FIGURATE_HPP

#include <cmath>


	template<typename Number>
	Number tria(const Number &n) {
		return (n * (n + 1)) / 2;
	}

	template<typename Number>
	Number tetra(const Number &n) {
		return n * n;
	}

	template<typename Number>
	Number penta(const Number &n) {
		return (n * (3*n - 1)) / 2;
	}
	
	template<typename Number>
	Number hexa(const Number &n) {
		return n * (2*n - 1);
	}

	template<typename Number>
	Number hepta(const Number &n) {
		return n * (5*n - 3) / 2;
	}

	template<typename Number>
	Number octa(const Number &n) {
		return n * (3*n - 2);
	}

	template<typename Number>
	Number isTria(const Number &t) {
		Number n = static_cast<Number>(((-1 + std::sqrt(1 + 8*t)) / 2) + 0.5);
		if (tria(n) == t)
			return n;
		return 0;
	}

	template<typename Number>
	Number isTetra(const Number &t) {
		Number n = static_cast<Number>(std::sqrt(t) + 0.5);
		if (tetra(n) == t)
			return n;
		return 0;
	}

	template<typename Number>
	Number isPenta(const Number &t) {
		Number n = static_cast<Number>(((1 +  std::sqrt(1 + 24*t)) / 6) + 0.5);
		if (penta(n) == t)
			return n;
		return 0;
	}

	template<typename Number>
	Number isHexa(const Number &t) {
		Number n = static_cast<Number>(((1 + std::sqrt(1 + 8*t)) / 4) + 0.5);
		if (hexa(n) == t)
			return n;
		return 0;
	}

	template<typename Number>
	Number isHepta(const Number &t) {
		Number n = static_cast<Number>(((3 + std::sqrt(9 + 40*t)) / 10) + 0.5);
		if (hepta(n) == t)
			return n;
		return 0;
	}

	template<typename Number>
	Number isOcta(const Number &t) {
		Number n = static_cast<Number>(((2 + std::sqrt(4 + 12*t)) / 6) + 0.5);
		if (octa(n) == t)
			return n;
		return 0;
	}

#endif
