#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <gmpxx.h>
#include "primes.hpp"
#include "math.hpp"

namespace {
	typedef boost::uint64_t Number;
	typedef Primes_t<Number> P;

	P primes;

	Number sigma2(Number n) {
		P::Factors f = primes.factor(n);
		Number res = 1;
		P::Factors::const_iterator end = f.end();
		for (P::Factors::const_iterator i = f.begin(); i != end; ++i) {
			const Number p = i->first;
			const Number p2 = p * p;
			const Number k = i->second;
			Number z = p2;
			Number s = 1;
			for (Number j = 1; j <= k; ++j) {
				s += z;
				z *= p2;
			}
			res *= s;
		}
		return res;
	}

	bool check(Number n) {
		Number sig2 = sigma2(n);
		if (isSquare(sig2)) {
			std::cout << n << " -> " << sig2 << std::endl;
			return true;
		}
		return false;
	}
}

int main(int argc, const char **argv) {

	Number n = 64000000LL;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}

	Number res = 0;

	for (Number i = 1; i < n; ++i) {
		if (check(i)) {
			res += i;
		}
	}
	std::cout << res << std::endl;
	return 0;
}
