#ifndef MAGIC_NUMBER_HPP
#define MAGIC_NUMBER_HPP

#include <iostream>
#include <vector>
#include <boost/lexical_cast.hpp>

template <typename DigitType_ = uint64_t>
class MagicNumber {
public:
    typedef DigitType_ DigitType;

    template <typename ValueType = DigitType>
    MagicNumber(ValueType value = 0, DigitType base = 10) : base_(base) {
        set(value);
    }

    template <typename ValueType>
    ValueType get(DigitType base) const {
        ValueType res = 0;
        ValueType mult = 1;
        for (auto d : digits_) {
            res += d * mult;
            mult *= base;
        }
        return res;
    }

    template <typename ValueType>
    ValueType get() const {
        return get<ValueType>(base_);
    }

    template <typename ValueType>
    MagicNumber& set(ValueType value) {
        digits_.clear();
        while (value > 0) {
            digits_.push_back(value % base_);
            value /= base_;
        }
        return *this;
    }

    MagicNumber& base(DigitType base) {
        base_ = base;
        return *this;
    }

    DigitType base() const {
        return base_;
    }

    template <typename ValueType>
    MagicNumber& operator=(ValueType value) {
        return set(value);
    }

    friend std::ostream& operator<<(std::ostream& os, const MagicNumber& mn) {
        os << '[';
        bool first = true;
        for (auto it = mn.digits_.rbegin(), end = mn.digits_.rend(); it != end; ++it) {
            if (first) {
                first = false;
            }
            else {
                os << ' ';
            }
            os << (*it);
        }
        os << "](" << mn.base_ << ") = " << mn.get<uint64_t>();
        return os;
    }

private:
    typedef std::vector<DigitType> Digits;
    DigitType base_;
    Digits digits_;

};

#endif
