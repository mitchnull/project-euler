#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include <vector>

namespace {
	class BSet : public std::vector<bool> {
		typedef std::vector<bool> Base_;
	public:
		BSet(int numBits, int numSet = 0) : Base_(numBits) {
			std::fill(rbegin(), rbegin() + numSet, true);
		}

		BSet(const BSet &a, const BSet &b) : Base_(a) {
			// merge a and b. set 0's in a according to b
			const_iterator j = b.begin();
			for (iterator i = begin(); i != end() && j != b.end(); ++i)
				if (!(*i) && (*j++))
					*i = true;
		}
		bool allSet() const {
			const const_iterator &e = end();
			for (const_iterator i = begin(); i != e; ++i)
				if (!(*i))
					return false;
			return true;
		}
		friend std::ostream &operator<<(std::ostream &os, const BSet &bset) {
			for (BSet::const_iterator i = bset.begin(); i != bset.end(); ++i)
				std::cout << (*i ? '1' : '0');
			return os;
		}
	};
	typedef boost::int64_t Number;
	class Digits : public std::vector<int> {
		typedef std::vector<int> Base_;
	public:
		Digits() {}
		Digits(const BSet &aset) {
			for (BSet::size_type i = 0; i < aset.size(); ++i)
				if (aset[i])
					push_back(i+1);
		}

		Digits(const BSet &aset, const BSet &bset) {
			for (BSet::size_type i = 0, j = 0; i < aset.size() && j < bset.size(); ++i) {
				if (!aset[i] && bset[j++])
					push_back(i+1);
			}
		}
	};

	Number value(const Digits &d) {
		Number res = 0;
		for (Digits::const_iterator i = d.begin(); i != d.end(); ++i) {
			res *= 10;
			res += (*i);
		}
		return res;
	}

	Number check(const BSet &as, const Digits &ad, const BSet &bs, const Digits &bd) {
		BSet used(as, bs);
		Number res = value(ad) * value(bd);
//		std::cout << value(ad) << " * " << value(bd) << " = " << res << std::endl;
		Number tmp = res;
		while (tmp != 0) {
			const int ldi = tmp % 10 - 1;
			if (ldi < 0 || ldi >= static_cast<int>(used.size()) || used[ldi])
				return 0;
			used[ldi] = true;
			tmp /= 10;
		}
		if (used.allSet())
			return res;
		return 0;
	}
}

int
main(int argc, const char **argv) {
	int n = 9;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	std::set<Number> mmpPandigitals;
	Number res = 0;
	const int maxLen = (n + 1) / 2;

	for (int al = 1; al < maxLen; ++al) {
		BSet as(n, al);
		const int maxBLen = std::min(maxLen, n - 2 * al + 1);
		do {
			Digits ad(as);
			for (int bl = 1; bl < maxBLen; ++bl) {
				BSet bs(n - al, bl);
				do {
					if (ad[0] == 0) {
						if (ad.size() == 1)
							break;
						std::swap(ad[0], ad[1]);
					}
					Digits bd(as, bs);
					do {
						if (bd[0] == 0) {
							if (bd.size() == 1)
								break;
							std::swap(bd[0], bd[1]);
						}
						do {
							Number p = check(as, ad, bs, bd);
							if (p && mmpPandigitals.insert(p).second) {
								res += p;
								std::cout << value(ad) << " * " << value(bd) << " = " << p << std::endl;
							}
						} while (std::next_permutation(bd.begin(), bd.end()));
					} while (std::next_permutation(ad.begin(), ad.end()));
				} while (std::next_permutation(bs.begin(), bs.end()));
			}
		} while (std::next_permutation(as.begin(), as.end()));
	}
	std::cout << res << std::endl;
	return 0;
}
