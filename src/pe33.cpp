#include <iostream>
#include <sstream>
#include <boost/cstdint.hpp>
#include <boost/rational.hpp>

typedef int Number;
typedef boost::rational<Number> Rat;

namespace {
	bool check1(Rat x, Number n, Number d) {
		if (n >= d)
			return false;
		if (Rat(n, d) == x) {
			std::cout << n << '/' << d << " == " << x << std::endl;
			return true;
		}
		return false;
	}

	Rat check(Number i, Number j, Number k) {
		Rat res(i, k);
		if (check1(res, 10*i+j, 10*j + k)
				|| check1(res, 10*i + j, 10*k + j)
				|| check1(res, 10*j + i, 10*j + k)
				|| check1(res, 10*j + i, 10*k + j)) {
			return res;
		}
		return Rat(1);
	}
}

int main(int argc, const char **argv) {
	Rat res(1);
	for (Number i = 1; i < 10; ++i) {
		for (Number j = 1; j < 10; ++j) {
			if (i == j)
				continue;
			for (Number k = 1; k < 10; ++k) {
				if (k == j)
					continue;
				res *= check(i, j, k);
			}
		}
	}
	std::cout << "PROD = " << res << std::endl;
	std::cout << "DENOM = " << res.denominator() << std::endl;
	return 0;
}
