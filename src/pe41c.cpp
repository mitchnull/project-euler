#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include <vector>
#include "primes.hpp"

namespace {
	int parse(const int *digits, int n) {
		int res = 0;
		const int *end = digits + n;
		for (; digits != end; ++digits) {
			res *= 10;
			res += *digits;
		}
		return res;
	}

}

int
main(int argc, const char **argv) {
	int n = 9;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}

	int digits[9] = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };

	for (int i = n; i > 0; --i) {
		if (((i*(i+1)) / 2) % 3 == 0) {
			std::cout << "skipping " << i << std::endl;
			continue;
		}
		do {
			int p = parse(digits + 9 - i, i);
			if (isPrime(p)) {
				std::cout << p << std::endl;
				return 0;
			}
		} while (std::prev_permutation(digits + 9 - i, digits + 9));
	}
	return 0;
}
