#include <iostream>
#include <sstream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include "primes.hpp"
#include <vector>
#include <algorithm>
#include <limits>
#include <utility>
#include <set>
#include "math.hpp"

namespace {
	typedef boost::uint64_t Number;
	typedef std::pair<Number, Number> NumPair;

	class SparseBitVec {
		typedef std::set<NumPair> Data;
		Data d_;
	public:
		typedef Data::const_iterator const_iterator;

		bool get(const Number &p1, const Number &p2) {
			return d_.find(std::make_pair(p1, p2)) != d_.end();
		}
		void set(const Number &p1, const Number &p2) {
			d_.insert(std::make_pair(p1, p2));
		}
		const_iterator begin() const { return d_.begin(); }
		const_iterator end() const { return d_.end(); }
		const_iterator upper_bound(Number first) {
			return d_.upper_bound(NumPair(first, 0));
		}
	};

	typedef std::set<Number> NumSet;

	typedef Primes_t<Number> P;
	P primes;
	SparseBitVec checks;
	NumSet checked;

	bool check(Number p1, Number p2) {
		for (int n = numDigits(p2) ; n; --n) {
			p1 *= 10;
		}
		return primes.isPrime(p1+p2);
	}

	bool check2(Number p1, Number p2) {
		if (p1 == p2)
			return false;
		if (p1 < p2)
			return checks.get(p1, p2);
		else
			return checks.get(p2, p1);
	}

}

int main(int argc, const char **argv) {

	Number n = 10000;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}

	Number res = std::numeric_limits<Number>::max();

	primes.sieve(n);

	std::cout << "sieve done: " << primes.size() << std::endl;
	Number p1;
	for (Primes::size_type i = 1; (p1 = primes[i]) < n; ++i) {
		Number p2;
		bool found = false;
		for (Primes::size_type j = i + 1; (p2 = primes[j]) < n; ++j) {
			if (check(p1, p2) && check(p2, p1)) {
				found = true;
				checks.set(p1, p2);
				checked.insert(p2);
			}
		}
		if (found)
			checked.insert(p1);
	}
	std::cout << "checks done" << std::endl;
	SparseBitVec::const_iterator end = checks.end();
	for (SparseBitVec::const_iterator i = checks.begin(); i != end; ++i) {
		const NumPair &ii = *i;
		for (SparseBitVec::const_iterator j = checks.upper_bound(ii.second); j != end; ++j) {
			const NumPair &jj = *j;
			if (check2(ii.first, jj.first)
					&& check2(ii.first, jj.second)
					&& check2(ii.second, jj.first)
					&& check2(ii.second, jj.second)) {
				for (NumSet::const_iterator k = checked.upper_bound(std::max(ii.second, jj.second)); k != checked.end(); ++k) {
					Number kk = *k;
					if (check2(kk, ii.first)
							&& check2(kk, ii.second)
							&& check2(kk, jj.first)
							&& check2(kk, jj.second)) {
						Number cs = ii.first + ii.second + jj.first + jj.second + kk;

						if (cs < res) {
							res = cs;
						}
						std::cout << ii.first << ", " << ii.second << ", " << jj.first << ", " << jj.second << ", " << kk << ": " << cs << std::endl;
					}
				}
			}
		}
	}
	std::cout << res << std::endl;
	return 0;
}
