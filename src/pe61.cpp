#include <iostream>
#include <algorithm>
#include <boost/cstdint.hpp>
#include <boost/lexical_cast.hpp>
#include <set>
#include <cctype>
#include "figurate.hpp"
#include <vector>


namespace {
	typedef int Number;
	typedef std::vector<Number> NumVec;
	typedef std::vector<NumVec*> NVPVec;
	typedef std::set<Number> NumSet;

	typedef Number (*FF)(const Number &);

	template<typename T, size_t n>
	char (& as_char_array(const T (&)[n]) )[n];

	FF figFuncs[] = {
		isTria<Number>,
		isTetra<Number>,
		isPenta<Number>,
		isHexa<Number>,
		isHepta<Number>,
		isOcta<Number>
	};

	struct NFI {
		Number num;
		std::size_t fig;
		Number index;

		NFI() {}
		NFI(Number n, Number f, Number i) : num(n), fig(f), index(i) {}
	};

	typedef std::vector<NFI> NFIVec;

	const std::size_t NumFuncs = sizeof(as_char_array(figFuncs));

	Number makeNum(Number a, Number b) {
		return 100 * a + b;
	}

	class Cache : public NVPVec {
		typedef NVPVec Base_;

		std::size_t n_;

		NumVec *calc(Number ab) {
			NumVec *res = 0;
			for (std::size_t i = 0; i < n_; ++i) {
				FF f = figFuncs[i];
				Number n = f(ab);
				if (n) {
					if (!res)
						res = new NumVec(n_);
					(*res)[i] = n;
				}
			}
			return res;
		}

	public:
		Cache(std::size_t n) : Base_(10000), n_(n) {
			std::cout << "caching...";
			for (Number a = 10; a < 100; ++a) {
				for (Number b = 10; b < 100; ++b) {
					Number ab = makeNum(a, b);
					(*this)[ab] = calc(ab);
				}
			}
			std::cout << " done." << std::endl;
		}

		~Cache() {
			std::cout << "cleanup...";
			for (NVPVec::iterator i = begin(); i != end() ; ++i)
				delete(*i);
			std::cout << " done." << std::endl;
		}
	};

	Cache cache(NumFuncs);

	void print(const NFIVec &lst) {
		Number sum = 0;
		for (NFIVec::const_iterator i = lst.begin(); i != lst.end(); ++i) {
			std::cout << "P" << (i->fig + 3) << "(" << i->index << "): " << i->num << std::endl;
			sum += i->num;
		}
		std::cout << "SUM = " << sum << std::endl;
	}


	bool isUsed(const NFIVec &lst, const NFI &nfi) {
		for (NFIVec::const_iterator i = lst.begin(); i != lst.end(); ++i)
			if (i->num == nfi.num || i->fig == nfi.fig || i->index == nfi.index)
				return true;
		return false;
	}

	bool check(NFI nfi, std::size_t n, NFIVec lst = NFIVec()) {
		lst.push_back(nfi);
		if (lst.size() == n - 1) {
			Number bc = makeNum(nfi.num % 100, lst.front().num / 100);
			NumVec *fnp = cache[bc];
			if (!fnp)
				return false;
			std::size_t fig = 0;
			for (NumVec::const_iterator i = fnp->begin(); fig < n && i != fnp->end(); ++i, ++fig) {
				if (!(*i))
					continue;
				const NFI nextnfi(bc, fig, *i);
				if (*i && !isUsed(lst, nextnfi)) {
					lst.push_back(nextnfi);
					print(lst);
//					return true;
					return false;
				}
			}
			return false;
		}
		Number b = nfi.num % 100;
		for (Number c = 10; c < 100; ++c) {
			Number bc = makeNum(b, c);
			NumVec *fnp = cache[bc];
			if (!fnp)
				continue;
			std::size_t fig = 0;
			for (NumVec::const_iterator i = fnp->begin(); fig < n && i != fnp->end(); ++i, ++fig) {
				if (!(*i))
					continue;
				const NFI nextnfi(bc, fig, *i);
				if (*i && !isUsed(lst, nextnfi)) {
					if (check(nextnfi, n, lst))
						return true;
				}
			}
		}
		return false;
	}
}

int
main(int argc, const char **argv) {
	std::size_t n = NumFuncs;
	if (argc > 1) {
		n = boost::lexical_cast<std::size_t>(argv[1]);
	}
	if (n > NumFuncs)
		n = NumFuncs;
	if (n < 2)
		n = 2;

	for (Number i = 1010; i < 9999; ++i) {
		if (cache[i] && (*cache[i])[n-1]) {
			if (check(NFI(i, n-1, (*cache[i])[n-1]), n))
				break;
		}
	}

	return 0;
}
