#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <boost/rational.hpp>
#include "cfract.hpp"
#include "math.hpp"

namespace {

	typedef mpz_class Number;
	typedef boost::rational<Number> Rat;
	typedef CFract_t<Number> CF;

	int sumDigits(const Number &n) {
		int res = 0;
		std::string ns = boost::lexical_cast<std::string>(n);
		for (std::string::const_iterator i = ns.begin(); i != ns.end(); ++i) {
			res += (*i) - '0';
		}
		return res;
	}

}

int main(int argc, const char **argv) {

	int n = 100;

	if (argc > 1) {
		n = boost::lexical_cast<int>(argv[1]);
	}

	std::vector<Number> f;
	f.reserve(n);
	int k = 0;
	for (Number i = 1; i < n; ++i) {
		if (i % 3 == 2)
			f.push_back(k += 2);
		else
			f.push_back(1);
	}
	CF cf(2, f);
	Rat r = cf.expand<Rat>(n - 1);

//	std::cout << cf << std::endl;
//	std::cout << r << std::endl;
	std::cout << sumDigits(r.numerator()) << std::endl;
	return 0;
}
