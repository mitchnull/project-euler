#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include "math.hpp"

namespace {
	typedef mpz_class Number;

	template <typename T>
	T rev(T n, const int base = 10) {
		T res = 0;
		while (n != 0) {
			res *= base;
			res += n % base;
			n /= base;
		}
		return res;
	}
	template <typename T>
	bool isPalindrome(const T &n, const int base = 10) {
		return rev(n, base) == n;
	}

	bool isLychrel(Number n, int k) {
		for (int i = 0; i < k; ++i) {
			n = n + rev(n);
			if (isPalindrome(n))
				return false;
		}
		return true;
	}
}


int main(int argc, const char **argv) {

	Number n = 10000;
	int k = 50;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}
	if (argc > 2) {
		k = boost::lexical_cast<int>(argv[2]);
	}

	int res = 0;
	for (Number i = 1; i < n; ++i) {
		if (isLychrel(i, k)) {
//			std::cout << i << std::endl;
			++res;
		}
	}
	std::cout << res << std::endl;
	return 0;
}
