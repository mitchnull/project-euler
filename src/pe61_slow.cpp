#include <iostream>
#include <algorithm>
#include <boost/cstdint.hpp>
#include <boost/lexical_cast.hpp>
#include <set>
#include <cctype>
#include "figurate.hpp"
#include <vector>


namespace {
	typedef int Number;
	typedef std::vector<Number> NumVec;

	typedef Number (*FF)(const Number &);


	template<typename T, size_t n>
	char (& as_char_array(const T (&)[n]) )[n];

	FF figFuncs[] = {
		isTria<Number>,
		isTetra<Number>,
		isPenta<Number>,
		isHexa<Number>,
		isHepta<Number>,
		isOcta<Number>
	};

	const std::size_t NumFuncs = sizeof(as_char_array(figFuncs));

	Number makeNum(Number a, Number b) {
		return 100 * a + b;
	}

	void printCheck(Number ab, const std::size_t numFuncs) {
		std::cout << ab << ":";
		for (std::size_t i = 0; i < numFuncs; ++i) {
			FF f = figFuncs[i];
			Number n = f(ab);
			if (n) {
				std::cout << " P(" << (i + 3) << ", " << n << ")";
			}
		}
		std::cout << std::endl;
	}

	bool check(std::set<Number> &fns, Number a, Number b, const std::size_t numFuncs) {
		bool res = false;
		for (std::size_t i = 0; i < numFuncs; ++i) {
			FF f = figFuncs[i];
			Number n = f(makeNum(a, b));
			if (n) {
				if (res || !fns.insert(n).second || !fns.insert(-i).second)
					return false;
				res = true;
			}
		}
		return res;
	}

	bool check(const NumVec &pairs) {
		const std::size_t numFuncs = pairs.size();
		static std::set<Number> fns;
		fns.clear();
		NumVec::const_iterator i = pairs.begin();
		NumVec::const_iterator j = pairs.begin();
		NumVec::const_iterator end = pairs.end();
		for (++j; j != end; ++i, ++j) {
			if (!check(fns, *i, *j, numFuncs))
				return false;
		}
		if (!check(fns, *i, pairs.front(), numFuncs))
			return false;
		return fns.size() == 2 * pairs.size();
	}

	void print(const NumVec &pairs) {
		NumVec::const_iterator i = pairs.begin();
		NumVec::const_iterator j = pairs.begin();
		NumVec::const_iterator end = pairs.end();
		Number sum = 0;
		for (++j; j != end; ++i, ++j) {
			Number ab = makeNum(*i, *j);
			sum += ab;
			std::cout << ab << " + ";
		}
		Number ab = makeNum(*i, pairs.front());
		sum += ab;
		std::cout << ab << " = " << sum << std::endl;
	}

	void printCheck(const NumVec &pairs) {
		NumVec::const_iterator i = pairs.begin();
		NumVec::const_iterator j = pairs.begin();
		NumVec::const_iterator end = pairs.end();
		for (++j; j != end; ++i, ++j) {
			Number ab = makeNum(*i, *j);
			printCheck(ab, pairs.size());
		}
		Number ab = makeNum(*i, pairs.front());
		printCheck(ab, pairs.size());
	}

	bool inc(NumVec &pairs) {
		for (NumVec::reverse_iterator i = pairs.rbegin(); i != pairs.rend(); ++i) {
			if (++(*i) < 100)
				return false;
			*i = 10;
		}
		return true;
	}
}

int
main(int argc, const char **argv) {
	std::size_t n = NumFuncs;
	if (argc > 1) {
		n = boost::lexical_cast<std::size_t>(argv[1]);
	}
	if (n > NumFuncs)
		n = NumFuncs;
	if (n < 2)
		n = 2;

	NumVec nums(n);
	std::fill(nums.begin(), nums.end(), 10);
	do {
//		std::cout << "checking: "; print(nums);
		if (check(nums)) {
			print(nums);
			printCheck(nums);
			return 0;
		}
	} while (!inc(nums));
	return 0;
}
