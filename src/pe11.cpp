#include <iostream>
#include <sstream>
#include <algorithm>
#include "matrix.hpp"

namespace {
	template<typename T>
	T prod(const Matrix<T> &m, int i, int j, int k, int di, int dj) {
		T res = 1;
		for (int d = 0; d < k; ++d) {
			res *= m(i,j);
			i += di;
			j += dj;
		}
		return res;
	}

	template<typename T>
	T maxSub(const Matrix<T> &m, int i, int j, int k) {
		return std::max(
			std::max(
				prod(m, i, j, k, +1, 0),
				prod(m, i, j, k, 0, +1)
				),
			std::max(
				prod(m, i, j, k, +1, +1),
				prod(m, i, j+k, k, +1, -1)
				)
			);
	}
}

int
main(int argc, const char **argv) {

	int n = 20;
	int k = 4;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	if (argc > 2) {
		std::stringstream s(argv[2]);
		s >> k;
	}
	Matrix<int> m(n, n);
	std::cin >> m;
	int res = 0;
	for (int i = 0; i <= n - k; ++i) {
		for (int j = 0; j <= n - k; ++j) {
			const int subm = maxSub(m, i, j, k);
			if (subm > res)
				res = subm;
		}
	}
	std::cout << res << std::endl;
	return 0;
}
