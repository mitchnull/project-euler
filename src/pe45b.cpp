#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include <vector>
#include "math.hpp"
#include <cmath>
#include <limits>


namespace {
	typedef boost::uint64_t Number;

	Number tria(Number n) {
		return (n * (n + 1)) / 2;
	}

	Number penta(Number n) {
		return (n * (3*n - 1)) / 2;
	}
	
	Number hexa(Number n) {
		return n * (2*n - 1);
	}

	bool isPenta(Number t) {
		Number n = static_cast<Number>(((1 +  std::sqrt(1 + 24 * t)) / 6) + 0.5);
		return penta(n) == t;
	}
}

int
main(int argc, const char **argv) {
	int n = 286;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	n = n / 2 + 1; // tria -> hexa
	Number ip = 1;
	Number pi = penta(ip);

	for (Number ih = n; ; ++ih) {
		const Number hi = hexa(ih);
		while (pi < hi)
			pi = penta(++ip);
		if (pi == hi) {
			std::cout << "T(" << (2 * ih - 1) << ") = P(" << ip << ") = H(" << ih << ") = " << hi << std::endl;
			return 0;
		}
	}

	return 0;
}
