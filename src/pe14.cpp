#include <iostream>
#include <sstream>
#include <boost/cstdint.hpp>
#include <map>

typedef boost::int64_t I64;

namespace {

	std::map<I64, I64> cache;
	
	I64 next(I64 n) {
		if (n % 2 == 0)
			return n / 2;
		return 3 * n + 1;
	}

	I64 runlen_recursive(I64 start) {
		if (start == 1)
			return 1;
		std::map<I64, I64>::iterator i = cache.find(start);
		if (cache.find(start) != cache.end()) {
			return i->second;
		}
		return (cache[start] = 1 + runlen_recursive(next(start)));

	}

	I64 runlen_dummy(I64 start) {
		int i = 1;
		while (start != 1) {
			++i;
			start = next(start);
		}
		return i;
	}

	I64 runlen(I64 start) {
		return runlen_dummy(start);
	}
}

int
main(int argc, const char **argv) {

	I64 n = 1000000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	const I64 res_recursive = runlen_recursive(n);
	const I64 res_dummy = runlen_dummy(n);
	std::cout << "RLR(" << n << ") = " << res_recursive << std::endl;
	std::cout << "RLD(" << n << ") = " << res_dummy << std::endl;
	I64 res = 0;
	I64 max = 0;
	for (int i = 1; i < n; ++i) {
		const I64 curr = runlen(i);
		if (curr > max) {
			max = curr;
			res = i;
		}
	}
	std::cout << "MRL(" << n << ") = RL(" << res << ") = " << max << std::endl;
	return 0;
}
