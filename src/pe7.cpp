#include <iostream>
#include <sstream>
#include "primes.hpp"

int
main(int argc, const char **argv) {

	boost::uint64_t n = 10001;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Primes primes;
	std::cout << "p(" << n << ") = " << primes[n - 1] << std::endl;
	return 0;
}
