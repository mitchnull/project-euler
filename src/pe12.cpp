#include <iostream>
#include <sstream>
#include "primes.hpp"

namespace {
	template<typename T>
	T numFactors(const typename Primes_t<T>::Factors &factors) {
		T res = 1;
		for (typename Primes_t<T>::Factors::const_iterator i = factors.begin(); i != factors.end(); ++i)
			res *= (i->second + 1);
		return res;
	}
}

int
main(int argc, const char **argv) {

	Primes::value_type n = 500;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Primes primes;
	Primes::value_type x = 0;
 	for (int i = 1; i >= 0; ++i) { // we'll stop on overflow
		x += i;
		Primes::Factors factors = primes.factor(x);
		Primes::value_type nf = numFactors<Primes::value_type>(factors);
		if (nf > n) {
			std::cout << "Factors(" << x << ") =";
			for (Primes::Factors::const_iterator it = factors.begin(); it != factors.end(); ++it) {
				std::cout << ' ' << it->first;
				if (it->second > 1)
					std::cout << '^' << it->second;
			}
			std::cout << std::endl;
			std::cout << "NumFactors(" << x << ") = " << nf << std::endl;
			return 0;
		}
	}
	std::cerr << "overflow!";
	return -1;
}
