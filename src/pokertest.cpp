#include <iostream>
#include <boost/lexical_cast.hpp>

#include "hand.hpp"

using namespace Poker;

namespace {
	void report(int winner, const Hand &wh, const Hand &lh) {
		std::cout << "P" << winner << " wins with " << wh.ranking() << ": " << wh.best() << " over " << lh.ranking() << ": "  << lh.best() << std::endl;
	}
}

int main(int argc, const char **argv) {

	int n = 5;
	if (argc > 1)
		n = boost::lexical_cast<int>(argv[1]);

	Cards c(n);

	while ((std::cin >> c)) {
		Hand h(c);
		std::cout << h.ranking() << ": " << h.best() << std::endl;
	}
	return 0;
}
