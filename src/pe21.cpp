#include <iostream>
#include <algorithm>
#include <sstream>
#include "primes.hpp"

namespace {
	template <typename T>
	struct Sum {
		T sum;

		Sum() : sum(0) {}
		void operator()(const T &i) {
			sum += i;
		}
	};
	template<typename T>
	T sumDivs(const typename Primes_t<T>::Factors &factors) {
		return std::for_each(factors.divsBegin(), factors.divsEnd(), Sum<T>()).sum;
	}
}

int
main(int argc, const char **argv) {

	Primes::value_type n = 10000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Primes primes;
	Primes::value_type res = 0;
	for (Primes::value_type i = 1; i <= n; ++i) {
		Primes::value_type sdi = sumDivs<Primes::value_type>(primes.factor(i)) - i;
		if (sdi <= i || sdi > n || sumDivs<Primes::value_type>(primes.factor(sdi)) - sdi != i)
			continue; 

		std::cout << "d(" << i << ") = " << sdi << " && d(" << sdi << ") = " << i << std::endl;
		res += (i + sdi);
	}
	std::cout << "SumD(" << n << ") = " << res << std::endl;
	return 0;
}
