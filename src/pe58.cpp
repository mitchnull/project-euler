#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include "primes.hpp"

namespace {
//	typedef boost::uint64_t Number;
	typedef unsigned int Number;

	Number c(Number d, int n) {
		return (d-2) * (d-2) + n*(d - 1);
	}
}

int main(int argc, const char **argv) {

	Number n = 1000000;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}

	Primes_t<Number> primes;
	primes.sieve(n);


	Number x = 0;
	for (Number i = 1; i < n; ++i)
		if (primes.check(i))
			++x;
	std::cout << "primes till " << n << ": " << x << std::endl;


	Number numPrimes = 0;
	Number numDiag = 1;
	Number cpc = 0;
	for (Number d = 3; ; d += 2) {
		for (int i = 1; i <= 4; ++i)
			if (primes.check(c(d, i)))
				++numPrimes;
		numDiag += 4;
		const Number pc = numPrimes * 100 / numDiag;
		if (pc != cpc) {
			std::cout << d << ": " << numPrimes << "/" << numDiag << " = " << pc << std::endl;
			cpc = pc;
		}
		if (pc < 10) {
			std::cout << d << std::endl;
			break;
		}
	}
	return 0;
}
