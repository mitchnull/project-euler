#include <iostream>
#include <sstream>
#include <algorithm>
#include "triangle.hpp"

typedef std::pair<int, int> Node;

namespace {
}

int
main(int argc, const char **argv) {

	int n = 15;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Triangle<int> t(n);
	std::cin >> t;

	Triangle<Node> w(n);
	for (int j = 0; j < n; ++j)
		w(n-1, j) = std::make_pair(t(n-1, j), -1);
	for (int i = n-2; i >= 0; --i) {
		for (int j = 0; j <= i; ++j) {
			const int left = w(i + 1, j).first;
			const int right = w(i + 1, j + 1).first;
			if (left > right) {
				w(i, j) = std::make_pair(left + t(i, j), 0);
			}
			else {
				w(i, j) = std::make_pair(right + t(i, j), 1);
			}
		}
	}
	std::cout << "MaxPath:";
	int j = 0;
	for (int i = 0; i < n; ++i) {
		Node wn = w(i, j);
		std::cout << ' ' << t(i,j);
		j += wn.second;
	}
	std::cout << std::endl;

	std::cout << "SumPath = " << w(0, 0).first << std::endl;
	return 0;
}
