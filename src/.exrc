if &cp | set nocp | endif
map  :bn
map  :bp
map  :tp
map  :tn
vmap ,s c[SNIP]
vmap ,c :`>a */`<i/* 
vmap ,C :`>a)`<i>(histatic_cast<
vmap ,p :`>a)`<i(
map ,t :!./runtest.sh %<
map ,F :1,$! fmt -78 -s
map ,G :!gdb %<_test
map ,T :!make %<_test.cpp %<_test
map ,I :call EditOtherExtension(".cpp", ".cc", ".c")
map ,i :call EditOtherExtension(".c", ".cpp", ".cc")
map ,H :call EditOtherExtension(".hpp", ".h")
map ,h :call EditOtherExtension(".h", ".hpp")
map ,m :make
map ,u :s,^//,,
nmap ,c 0i//
omap ,c 0i//
map Y y$
noremap ]] ][
noremap ][ ]]
let s:cpo_save=&cpo
set cpo&vim
nmap gx <Plug>NetrwBrowseX
omap q /[^a-z]
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#NetBrowseX(expand("<cWORD>"),0)
imap  :bn
imap  :bp
inoremap  
inoremap  
iabbr lm LOG((LOGID,
let &cpo=s:cpo_save
unlet s:cpo_save
set autoindent
set backspace=2
set backup
set comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,fb:-
set cscopeprg=/usr/bin/cscope
set cscopetag
set cscopeverbose
set fileencodings=utf-8,latin1
set formatoptions=tcroql
set helpheight=0
set helplang=en
set hidden
set history=50
set ignorecase
set keywordprg=man\ -a
set makeprg=make\ %<.o
set scrolljump=8
set shiftround
set shiftwidth=4
set shortmess=mrxfintI
set showcmd
set showmatch
set smartcase
set nostartofline
set tabstop=4
set tags=CTAGS,../CTAGS,../../CTAGS,../../../CTAGS,../../../../CTAGS,../../../../../CTAGS,./tags,./.tags,tags,.tags,/u/subversion/svn/TAGS
set textwidth=72
set title
set viminfo='50,f1,\"1024
set visualbell
" vim: set ft=vim :
