#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include <vector>
#include "math.hpp"
#include <cmath>
#include <limits>


namespace {
	typedef boost::uint64_t Number;

	Number penta(Number n) {
		return (n * (3*n - 1)) / 2;
	}

	bool isPenta(Number t) {
		Number n = static_cast<Number>(((1 +  std::sqrt(1 + 24 * t)) / 6) + 0.5);
		return penta(n) == t;
	}
}

int
main(int argc, const char **argv) {
	for (int k = 1; ; ++k) {
		for (int j = 1; j < k; ++j) {
			const Number pj = penta(j);
			const Number pk = penta(k);
			if (isPenta(pk + pj) && isPenta(pk - pj)) {
				std::cout << "|P(" << j << ") - P(" << k << ")| = " << (pk - pj) << std::endl;
				return 0;
			}
		}
	}
	return 0;
}
