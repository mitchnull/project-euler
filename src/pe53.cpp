#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <gmpxx.h>
#include "math.hpp"

namespace {
	typedef mpz_class Number;
}

int main(int argc, const char **argv) {

	Number n = 100;
	Number t = 1000000;

	if (argc > 1) {
		n = boost::lexical_cast<Number>(argv[1]);
	}
	if (argc > 2) {
		t = boost::lexical_cast<Number>(argv[2]);
	}

	Number res = 0;

	for (Number i = 1; i <= n; ++i) {
		for (Number j = 2; j < i; ++j) {
			if (nk(i, j) > t) {
				std::cout << "nk(" << i << ", " << j << ") = " << nk(i, j) << std::endl;
				++res;
			}
		}
	}
	std::cout << res << std::endl;
	return 0;
}
