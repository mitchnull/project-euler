#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include "math.hpp"

typedef boost::uint64_t Number;

namespace {

	template <typename T>
	T rev(T n, const int base = 10) {
		T res = 0;
		while (n != 0) {
			res *= base;
			res += n % base;
			n /= base;
		}
		return res;
	}
	template <typename T>
	bool isPalindrome(const T &n, const int base = 10) {
		return rev(n, base) == n;
	}
}

int
main(int argc, const char **argv) {
	Number n = 1000000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Number res = 0;
	for (Number i = 1; i < n; i += 2) {
		if (isPalindrome(i, 2) && isPalindrome(i, 10)) {
			std::cout << i << std::endl;
			res += i;
		}
	}
	std::cout << "SumPali2And10(" << n << ") = " << res << std::endl;
	return 0;
}
