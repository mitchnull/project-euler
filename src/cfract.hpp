#ifndef PE_CFRACT_HPP
#define PE_CFRACT_HPP

#include <boost/cstdint.hpp>
#include <sstream>
#include <vector>
#include <cmath>
#include <boost/rational.hpp>
#include "math.hpp"

template<typename Int = int>
class CFract_t {
	static const double Eps = 1E-6;
	typedef std::vector<Int> Fractions;
	Int int_;
	Fractions f_;
	int period_;

	enum { Prec = 256 };

	static bool isEq(const double a, const double b) {
		return (a < b) ? (b - a < Eps) : (a - b < Eps);
	}
	class DoubleEq {
		const double val_;
	public:
		DoubleEq(double val): val_(val) {}

		bool operator()(const double d) {
			return isEq(val_, d);
		}
	};

	struct MDA {
		Int m, d, a;

		MDA() {}
		MDA(Int mm, Int dd, Int aa) : m(mm), d(dd), a(aa) {}
		bool operator==(const MDA &o) {
			return m == o.m && d == o.d && a == o.a;
		}
	};

public:
	class SquareOf {
		Int v;
		SquareOf(Int n) : v(n) {}
		friend class CFract_t;
	};

	static SquareOf squareOf(Int n) { return SquareOf(n); }

	typedef typename Fractions::size_type size_type;

	CFract_t(Int i = 0, Fractions f = Fractions(), int period = 0) : int_(i), f_(f), period_(period) {}

	CFract_t(const SquareOf &r) : int_(isqrt(r.v)) {
		typedef std::vector<MDA> MDAVec;
		if (int_ * int_ == r.v) {
			period_ = 0;
			return;
		}
		MDAVec t;
		MDA mda(0, 1, int_);
		while (true) {
			MDA next;
			next.m = mda.d * mda.a - mda.m;
			next.d = (r.v - next.m * next.m) / mda.d;
			next.a = (int_ + next.m) / next.d;
			typename MDAVec::iterator p = std::find(t.begin(), t.end(), next);
			if (p != t.end()) {
				period_ = t.end() - p;
				return;
			}
			t.push_back(next);
			f_.push_back(next.a);
			mda = next;
		}
	}

	// TODO: negative values
	template<typename Numeric>
	CFract_t(boost::rational<Numeric> r) : int_(boost::rational_cast<Int>(r)) {
		r -= int_;
		while (r != 0) {
			r = 1 / r;
			const Int i = boost::rational_cast<Int>(r);
			f_.push_back(i);
			r -= i;
		}
		period_ = 0;
	}

	// TODO: negative values
	CFract_t(double r) : int_(static_cast<Int>(r)) {
		typedef std::vector<double> DoubleVec;
		DoubleVec t;
		r -= int_;
		while (r != 0) {
			t.push_back(r);
			r = 1 / r;
			const Int i = static_cast<Int>(r);
			f_.push_back(i);
			r -= i;
			DoubleVec::iterator p = std::find_if(t.begin(), t.end(), DoubleEq(r));
			if (p != t.end()) {
				period_ = t.end() - p;
				return;
			}
		}
		period_ = 0;
	}

	CFract_t(const std::string &str) {
		std::stringstream s(str);
		s >> (*this);
	}

	template<typename I2>
	CFract_t(const CFract_t<I2> &other) : int_(other.int_), f_(other.f_), period_(other.period_) {}

	template<typename I2>
	CFract_t &operator=(CFract_t<I2> other){
		int_ = other.int_;
		std::swap(f_, other.f_);
		period_ = other.period_;
		return *this;
	}

	template<typename I2>
	bool operator==(const CFract_t<I2> &other) const {
		return int_ == other.int_ && period_ == other.period_ && f_ == other.f_;
	}

	template<typename I2>
	bool operator!=(const CFract_t<I2> &other) const {
		return !operator==(other);
	}

	template<typename T>
	T expand(size_type prec) const {
		if (!period_ && prec > f_.size())
			prec = f_.size();
		if (!prec)
			return int_;
		T res = T(0);
		while (prec > 0) {
			res = 1 / (frac(--prec) + res);
		}
		return res + int_;
	}

	template<typename T>
	T expand() const {
		if (!period_)
			return expand<T>(f_.size());
		else
			return expand<T>(Prec);
	}

	int period() const { return period_; }

	friend std::ostream &operator<<(std::ostream &os, const CFract_t &cf) {
		if (cf.f_.size() == 0)
			return os << cf.int_;
		os << '[' << cf.int_ << "; ";
		bool isFirst = true;
		for (typename Fractions::const_iterator i = cf.f_.begin(); i != cf.f_.end(); ++i) {
			if (isFirst)
				isFirst = false;
			else
				os << ", ";
			os << *i;
		}
		if (cf.period_)
			os << "; " << cf.period_;
		return os << ']';
	}

	friend std::istream &operator>>(std::istream &is, CFract_t &cf) {
		char ch;
		CFract_t tmp;
		Int f;

		is >> ch;
		if (ch != '[') {
			is.putback(ch);
			is >> tmp.int_;
			goto finish;
		}
		is >> tmp.int_;
		is >> ch;
		if (ch == ']')
			goto finish;
		if (ch != ';') {
			is.setstate(std::ios::failbit);
			return is;
		}
		while (is) {
			is >> f;
			tmp.f_.push_back(f);
			is >> ch;
			if (ch == ';')
				break;
			if (ch == ',')
				continue;
			if (ch == ']')
				goto finish;
		}
		is >> tmp.period_;
		is >> ch;
		if (ch != ']' || (tmp.period_ > 0 && static_cast<typename Fractions::size_type>(tmp.period_) > tmp.f_.size()))
			is.setstate(std::ios::failbit);
finish:
		if (is) {
			if (tmp.period_ == 0 && tmp.f_.size() > 0) {
				if (tmp.f_.back() == 1) {
					tmp.f_.pop_back();
					if (tmp.f_.size() == 0)
						++tmp.int_;
					else
						++tmp.f_.back();
				}
			}
			cf.int_ = tmp.int_;
			std::swap(cf.f_, tmp.f_);
			cf.period_ = tmp.period_;
		}
		return is;
	}
private:
	Int frac(size_type i) const {
		if (period_ > 0) {
			const size_type ps = f_.size() - period_;
			if (i < ps)
				return f_[i];
			i -= ps;
			return f_[ps + i % f_.size()];
		}
		if (i >= f_.size())
			return 0;
		return f_[i];
	}
};

typedef CFract_t<> CFract;

#endif
