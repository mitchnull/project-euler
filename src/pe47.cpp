#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include <map>
#include <vector>
#include "primes.hpp"
#include "math.hpp"


namespace {
	typedef boost::uint64_t Number;

	Primes_t<Number> primes;
	typedef std::vector<Primes::Factors> FactVec;
	FactVec fv;
	Number n;

	bool check() {
		std::set<Primes::Factors::value_type> allfact;
		for (FactVec::const_iterator i = fv.begin(); i != fv.end(); ++i) {
			if (i->size() != n)
				return false;
			allfact.insert(i->begin(), i->end());
		}
		return allfact.size() == fv.size() * fv.size();
	}
}

int
main(int argc, const char **argv) {
	n = 4;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Number i;
	for (i = 0; i < n; ++i) {
		fv.push_back(primes.factor(i));
	}
	while (true) {
		++i;
		fv[i % n] = primes.factor(i);
		if (check()) {
			std::cout << i - n + 1 << std::endl;
			return 0;
		}
	}
	return 0;
}
