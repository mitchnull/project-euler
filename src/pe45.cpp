#include <iostream>
#include <algorithm>
#include <sstream>
#include <boost/cstdint.hpp>
#include <set>
#include <vector>
#include "math.hpp"
#include <cmath>
#include <limits>


namespace {
	typedef boost::uint64_t Number;

	Number tria(Number n) {
		return (n * (n + 1)) / 2;
	}

	Number penta(Number n) {
		return (n * (3*n - 1)) / 2;
	}
	
	Number hexa(Number n) {
		return n * (2*n - 1);
	}

	bool isPenta(Number t) {
		Number n = static_cast<Number>(((1 +  std::sqrt(1 + 24 * t)) / 6) + 0.5);
		return penta(n) == t;
	}
}

int
main(int argc, const char **argv) {
	int n = 286;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Number ip = 1;
	Number pi = penta(ip);
	Number ih = 1;
	Number hi = hexa(ih);

	for (Number it = n; ; ++it) {
		const Number ti = tria(it);
		while (pi < ti)
			pi = penta(++ip);
		while (hi < ti)
			hi = hexa(++ih);
		if (ti == pi && ti == hi) {
			std::cout << "T(" << it << ") = P(" << ip << ") = H(" << ih << ") = " << ti << std::endl;
			return 0;
		}
	}

	return 0;
}
