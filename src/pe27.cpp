#include <iostream>
#include <algorithm>
#include <sstream>
#include "primes.hpp"
#include <set>

typedef boost::int64_t Number;

namespace {
	Primes_t<Number> primes;

	template<typename T>
	T primeRunLen(T a, T b) {
		T k = 0;
		while (primes.check(k*k + a*k + b))
			++k;
		return k;
	}
	template<typename T>
	void printRun(T a, T b) {
		std::cout << "GEN(n^2 + " << a << "n + " << b << "):";
		T k = 0;
		T p;
		while (primes.check((p = k*k + a*k + b))) {
			std::cout << ' ' << p;
			++k;
		}
		std::cout << std::endl;
	}
}

int
main(int argc, const char **argv) {

	Number n = 1000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}
	Number ma(0), mb(0), mrl(0), b;
	for(Primes::size_type i = 0; (b = primes[i]) < n; ++i) {
		for (Number a = (-n + 1); a < n; ++a) {
			Number crl = primeRunLen(a, b);
			if (crl > mrl) {
				ma = a;
				mb = b;
				mrl = crl;
			}
		}
	}

	std::cout << "PRL(n^2 + " << ma << "n + " << mb << ") = " << mrl << ", a*b = " << ma * mb << std::endl;
	printRun(ma, mb);
	return 0;
}
