#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>
#include <boost/lexical_cast.hpp>

namespace {
	void print(int *begin, int *end) {
		for(; begin != end; ++begin) {
			std::cout << *begin;
		}
		std::cout << std::endl;
	}
}

int
main(int argc, const char **argv) {

	int n = 1000000;
	if (argc > 1) {
		std::stringstream s(argv[1]);
		s >> n;
	}

	int digits[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	for (int i = 1; i < n; ++i)
		std::next_permutation(digits, digits + 10);
	std::cout << "Perm(" << n << ") = ";
	print(digits, digits + 10);
	return 0;
}
