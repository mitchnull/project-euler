#include <iostream>
#include <iomanip>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <gmpxx.h>
#include <cmath>
#include "math.hpp"

namespace {
	typedef double Number;

	int sgn(double d) {
		if (d < 0)
			return -1;
		else
			return 1;
	}

	Number u(const Number &r, int k) {
		return (900 - 3*k) * pow(r, k - 1);
	}

	Number s1(const Number &r, int n) {
		Number res = 0;
		for (int k = 1; k <= n; ++k)
			res += u(r, k);
		return res;
	}

	Number s(const Number &r, int n) {
		Number rpow = 1;
		int firstPart = (900 - 3);
		Number res = firstPart;
		for (int k = 2; k <= n; ++k) {
			firstPart -= 3;
			rpow *= r;
			res += firstPart * rpow;
		}
		return res;
	}

	Number slim(const Number &r, int n, const Number &target) {
		Number rpow = 1;
		int firstPart = (900 - 3);
		Number res = firstPart;
		for (int k = 2; k <= n; ++k) {
			firstPart -= 3;
			rpow *= r;
			res += firstPart * rpow;
			if (k > 301 && res < target)
				return target - 1;
		}
		return res;
	}


	void testSig(const Number &r, int n) {
		bool pos = true;
		for (int k = 1; k <= n; ++k) {
			bool currpos = (u(r, k) >= 0);
			if (currpos != pos) {
				std::cout << k << (currpos ? '+' : '-') << std::endl;
				pos = currpos;
			}
		}
	}

	void test(const Number &r, int n) {
		std::cout << "u(" << r << ", " << n <<") = " << u(r, n) << std::endl;
		std::cout << "s1(" << r << ", " << n <<") = " << s1(r, n) << std::endl;
		std::cout << "s(" << r << ", " << n <<") = " << s(r, n) << std::endl;
	}
	void test(const Number &r, int n, const Number &target) {
		Number srn = s(r, n);
		Number srnlim = slim(r, n, target);
		Number diff = target - srn;
		std::cout << target << ": s(" << r << ", " << n <<") = " << s(r, n) << ", diff: " << diff << ", " << (target - srnlim) << std::endl;
	}
}

int main(int argc, const char **argv) {

	const Number eps = boost::lexical_cast<Number>("1E-15");
	int n = 5000;
	Number target = boost::lexical_cast<Number>("-600000000000");

	if (argc > 1) {
		n = boost::lexical_cast<int>(argv[1]);
	}
	if (argc > 2) {
		target = boost::lexical_cast<Number>(argv[2]);
	}
	Number rl = 1;
	Number rr = 2;
	while (rr - rl > eps) {
		Number med = (rl + rr) / 2;
		Number srn = slim(med, n, target);
		Number diff = target - srn;
		if (sgn(diff) <= 0) {
			rl = med;
		}
		else {
			rr = med;
		}
	}
	Number srn = s(rl, n);
	std::cout << "s(" << std::setprecision(12) << std::fixed << rl << ", " << n << ") = " << srn << ", diff: " << (target - srn) << std::endl;
	return 0;
}
