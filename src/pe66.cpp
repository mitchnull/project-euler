#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include "math.hpp"
#include "cfract.hpp"

namespace {
	typedef mpz_class Number;
//	typedef boost::uint64_t Number;
	typedef boost::rational<Number> Rat;
	typedef CFract_t<Number> CF;
}

int main(int argc, const char **argv) {

	int n = 1000;

	if (argc > 1) {
		n = boost::lexical_cast<int>(argv[1]);
	}

	Number maxX = 0;
	int maxD = 0;
	for (int d = 2; d <= n; ++d) {
		if (isSquare(d))
			continue;
		CF cf(CF::squareOf(d));
		for (int i = 0; ; ++i) {
			const Rat r = cf.expand<Rat>(i);
			const Number x = r.numerator();
			const Number y = r.denominator();
			if (x*x - d * y * y == 1) {
				std::cout << cf.period() << ": " << x << "^2 - " << d << "*" << y << "^2 = 1" << std::endl;
				if (x > maxX) {
					maxX = x;
					maxD = d;
				}
				break;
			}
		}
	}
	std::cout << maxD << std::endl;
	return 0;
}
