#ifndef POKER_CARD_HPP
#define POKER_CARD_HPP

#include <iostream>

namespace Poker {

	class Card {
	public:
		typedef enum { NullSuit, Spades, Hearts, Diamonds, Clubs } Suit;
		typedef enum { NullRank, Deuce = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace } Rank;

		Rank rank;
		Suit suit;

		Card(Rank r = NullRank, Suit s = NullSuit) : rank(r), suit(s) {}

		Card(const std::string &s) : rank(toRank(s[0])), suit(toSuit(s[1])) {}

		friend std::ostream &operator<<(std::ostream &os, const Card &card) {
			return os << card.rank << card.suit;
		}

		friend std::istream &operator>>(std::istream &is, Card &card) {
			return is >> card.rank >> card.suit;
		}

		friend std::ostream &operator<<(std::ostream &os, const Rank &rank) {
			switch (rank) {
				case Deuce: case Three: case Four: case Five:
				case Six: case Seven: case Eight: case Nine:
					return os << static_cast<char>('0' + rank);
				case Ten:
					return os << 'T';
				case Jack:
					return os << 'J';
				case Queen:
					return os << 'Q';
				case King:
					return os << 'K';
				case Ace:
					return os << 'A';
				case NullRank:
					break;
			}
			return os << 'X';
		}

		friend std::istream &operator>>(std::istream &is, Rank &rank) {
			char c;
			is >> c;
			rank = toRank(c);
			return is;
		}

		friend std::ostream &operator<<(std::ostream &os, const Suit &suit) {
			switch (suit) {
				case Spades:
					return os << 's';
				case Hearts:
					return os << 'h';
				case Diamonds:
					return os << 'd';
				case Clubs:
					return os << 'c';
				case NullSuit:
					break;
			}
			return os << 'x';
		}

		friend std::istream &operator>>(std::istream &is, Suit &suit) {
			char c;
			is >> c;
			suit = toSuit(c);
			return is;
		}

		static Suit toSuit(char ch) {
			switch (ch) {
				case 's': case 'S':
					return Spades;
				case 'h': case 'H':
					return Hearts;
				case 'd': case 'D':
					return Diamonds;
				case 'c': case 'C':
					return Clubs;
			}
			return NullSuit;
		}

		static Rank toRank(char ch) {
			switch (ch) {
				case '2': return Deuce;
				case '3': return Three;
				case '4': return Four;
				case '5': return Five;
				case '6': return Six;
				case '7': return Seven;
				case '8': return Eight;
				case '9': return Nine;
				case 't': case 'T':
					return Ten;
				case 'j': case 'J':
					return Jack;
				case 'q': case 'Q':
					return Queen;
				case 'k': case 'K':
					return King;
				case 'a': case 'A':
					return Ace;
			}
			return NullRank;
		}

		operator bool() const {
			return rank != NullRank && suit != NullSuit;
		}

		static bool lower(const Card &c, const Card &oc) {
			return c.rank < oc.rank;
		}

		static bool higher(const Card &c, const Card &oc) {
			return c.rank > oc.rank;
		}

		friend bool operator<(const Card &c, const Card &oc) {
			if (c.rank < oc.rank)
				return true;
			if (c.rank > oc.rank)
				return false;
			return c.suit < oc.suit;
		}

		friend bool operator>(const Card &c, const Card &oc) {
			if (c.rank > oc.rank)
				return true;
			if (c.rank < oc.rank)
				return false;
			return c.suit > oc.suit;
		}

		class IsSameRank {
			Rank rank_;
		public:
			IsSameRank(Rank rank) : rank_(rank) {}
			bool operator()(const Card &card) { return card.rank == rank_; }
		};

		class IsSameSuit {
			Suit suit_;
		public:
			IsSameSuit(Suit suit) : suit_(suit) {}
			bool operator()(const Card &card) { return card.suit == suit_; }
		};
	};
}
#endif
