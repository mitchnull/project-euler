#include <algorithm>
#include <iostream>
#include <tuple>
#include "matrix.hpp"

using uchar = unsigned char;
using uint = unsigned int;

using M = Matrix<uchar>;

static const uint N = 1025;

uint
getDmgAt(const M &m, uint x, uint y, uint d) {
    uint sx = (x > d) ? x - d : 0;
    uint ex = x + d + 1;
    if (ex > m.rows()) {
        ex = m.rows();
    }
    uint sy = (y > d) ? y - d : 0;
    uint ey = y + d + 1;
    if (ey > m.cols()) {
        ey = m.cols();
    }
    uint res = 0;
    for (uint i = sx; i < ex; ++i) {
        for (uint j = sy; j < ey; ++j) {
            res += m(i, j);
        }
    }
    return res;
}

std::tuple<uint, uint, uint>
findMaxDmgPos(const M &m, uint d) {
    uint rx = 0;
    uint ry = 0;
    uint rdmg = 0;
    for (uint x = 0; x < m.rows(); ++x) {
        for (uint y = 0; y < m.cols(); ++y) {
            uint cdmg = getDmgAt(m, x, y, d);
            if (cdmg > rdmg) {
                rdmg = cdmg;
                rx = x;
                ry = y;
            }
        }
    }
    return std::make_tuple(rx, ry, rdmg);
}

int
main(int argc, const char **argv) {
    uint numScen;
    std::cin >> numScen;
    while (numScen-- > 0) {
        M m(N, N);
        uint d;
        uint numPops;
        std::cin >> d >> numPops;
        while (numPops-- > 0) {
            uint x, y, sz;
            std::cin >> x >> y >> sz;
            m(x, y) = sz;
        }
        uint x, y, dmg;
        std::tie(x, y, dmg) = findMaxDmgPos(m, d);
        std::cout << x << ' ' << y << ' ' << dmg << "\n";
    }
}

