#!/usr/bin/perl -w
#

my $n = $ARGV[0] || 3;
my $b = 10 ** ($n - 1);
my $e = 10 ** ($n);

my $max = 0;
my $x = 0;
my $y = 0;

sub ispalindrome($) {
	my ($a) = @_;
	my $b = reverse($a);
	return ($a == $b);
}

for (my $i = $b; $i < $e; ++$i) {
	for (my $j = $b; $j <= $i; ++$j) {
		my $z = $i * $j;
		if ($z > $max && ispalindrome($z)) {
			$max = $z;
			$x = $i;
			$y = $j;
		}
	}
}
printf("%d * %d = %d\n", $x, $y, $max);
