#!/usr/bin/perl -w
#

my $n = $ARGV[0] || 20;

my $ppows = {};

sub max($$) {
	my ($a, $b) = @_;
	return $a > $b ? $a : $b;
}

sub getppows($) {
	my ($x) = @_;

	my $res = {};
outer: while (1) {
		for (my $i = 2; $i < $x; ++$i) {
			if ($x % $i == 0) {
				my $ppow = $res->{$i} || 0;
				$res->{$i} = ++$ppow;
				$x /= $i;
				next outer;
			}
		}
		my $ppow = $res->{$x} || 0;
		$res->{$x} = ++$ppow;
		return $res;
	}
}

for (my $i = 2; $i <= $n; ++$i) {
	my $ipp = getppows($i);
	foreach my $k (keys %{$ipp}) {
		my $ppow = $ppows->{$k} || 0;
		$ppows->{$k} = max($ppow, $ipp->{$k})
	}
}

my $mul = "";
my $res = 1;
foreach my $k (sort({$a <=> $b} keys %{$ppows})) {
	my $pp = $ppows->{$k};
	if ($pp > 1) {
		printf("%s%d^%d", $mul, int($k), $ppows->{$k});
	}
	else {
		printf("%s%d", $mul, int($k));
	}
	$mul = " * ";
	$res *= ($k ** $ppows->{$k});
}
printf(" = %d\n", $res);

