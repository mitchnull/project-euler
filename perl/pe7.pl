#!/usr/bin/perl -w
#

my $n = $ARGV[0] || 10001;

my $primes = [];
my $lastChecked = 1;

sub checkPrime($) {
	my ($x) = @_;
	my $y = sqrt($x);
	foreach my $p (@{$primes}) {
		last if ($p > $y);
		return 0 if ($x % $p == 0);
	}
	$primes->[scalar(@{$primes})] = $x;
	return 1;
}

sub isPrime($) {
	my ($x) = @_;

	while ($lastChecked < $x) {
		checkPrime(++$lastChecked);
	}
	return checkPrime($x);
}

my $i = 1;
my $c = 0;
while ($c < $n) {
	$c += checkPrime(++$i);
}
printf("p(%d) = %d\n", $n, $i);
