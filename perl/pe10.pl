#!/usr/bin/perl -w
#

my $n = $ARGV[0] || 2000000;

my $primes = [];
my $lastChecked = 1;

sub checkPrime($) {
	my ($x) = @_;
	my $y = sqrt($x);
	foreach my $p (@{$primes}) {
		last if ($p > $y);
		return 0 if ($x % $p == 0);
	}
	$primes->[scalar(@{$primes})] = $x;
	return 1;
}

sub isPrime($) {
	my ($x) = @_;

	while ($lastChecked < $x) {
		checkPrime(++$lastChecked);
	}
	return checkPrime($x);
}

my $sum = 0;
for (my $i = 2; $i < $n; ++$i) {
	$sum += $i if (checkPrime($i));
}
printf("SP(%d) = %d\n", $n, $sum);
