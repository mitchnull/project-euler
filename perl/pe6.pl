#!/usr/bin/perl -w
#
my $n = $ARGV[0] || 100;

my $squaresum = 0;
my $sumsquare = 0;
for (my $i = 1; $i <= $n; ++$i) {
	$squaresum += ($i * $i);
	$sumsquare += $i;
}
$sumsquare = $sumsquare * $sumsquare;

printf("n=%d, SUM(i^2) = %d, (SUM(i))^2 = %d, diff=%d\n", $n, $squaresum, $sumsquare, ($sumsquare - $squaresum));
