#!/usr/bin/perl -w
#

my $n = $ARGV[0] || 600851475143;

sub div($) {
	my ($x) = @_;
	my $l = sqrt($x);
	for (my $i = 2; $i < $l; ++$i) {
		return $x / $i if ($x % $i == 0);
	}
	return -1;
}

while(1) {
	my $nn = div($n);
	last if $nn == -1;
	$n = $nn;
}
printf("%d\n", $n);
