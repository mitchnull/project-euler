#!/usr/bin/perl -w
#

my $limit = $ARGV[0] || 4000000;
my $n0 = 1;
my $n1 = 1;
my $sum = 0;

while ($n1 < $limit) {
	$sum += $n1 if ($n1 % 2 == 0);
	my $tmp = $n0;
	$n0 = $n1;
	$n1 = $tmp + $n0;
}
printf("%d\n", $sum);
