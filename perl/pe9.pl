#!/usr/bin/perl -w
#

my $n = $ARGV[0] || 1000;

for (my $a = 1; $a < $n / 3; ++$a) {
	for (my ($b, $c) = ($a + 1, $n - ($a + $a + 1)); $b < $c; ++$b, --$c) {
		printf("n=%d: %d^2 + %d^2 = %d^2, a*b*c=%d\n",$n, $a, $b, $c, $a*$b*$c) if ($a * $a + $b * $b eq $c * $c);
	}
}
