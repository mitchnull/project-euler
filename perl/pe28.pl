#!/usr/bin/perl -w
#

my $n = $ARGV[0];

sub d($) {
	my ($n) = @_;

	return 0 if ($n <= 0);
	return 2 * $n -1;
}

sub D($) {
	my ($n) = @_;

	return 0 if ($n <= 0);
	return 1 if ($n == 1);
	return 16 * $n * $n - 28 * $n + 16;
}

sub R($) {
	my ($n) = @_;

	return 0 if ($n <= 0);
	return 16 * $n * ($n + 1) * (2*$n +1) / 6 - 28 * $n * ($n + 1) / 2 + 16 * $n - 3;
}

my $i = 1;
my $res = 0;
while ($i <= $n) {
	$res += D($i);
	++$i;
}
printf("n=%d, d=%d, res=%d, R=%d\n", $n, d($n), $res, R($n));
